
#include <iostream>
#include "cuda_runtime.h"
#include <thread>
#include <numeric>
#include <iomanip>
#include <string>
#include "TestBase.h"
#include "MultiplyTest.h"
#include "BandedMultiplyTest.h"

template<typename T>
T average(const std::vector<T>& vec)
{
	return std::accumulate(vec.begin(), vec.end(), T{}) / vec.size();
}

template<typename T>
double std_Dev(const std::vector<T>& vec)
{
	const T avg = average(vec);
	T var = 0;
	for (auto&& time : vec)
	{
		var += static_cast<T>(pow(time - avg, 2));
	}

	return sqrt(var / vec.size());
}

static Mode BandedMatrixMultiplyTests[] = {
	Mode::BandedNaive,
	Mode::BandedShiftMultiply64x1024,
	Mode::BandedShiftMultiply32x1024,
	Mode::BandedShiftMultiply64x512,
	Mode::BandedShiftMultiply32x512,
	Mode::BandedShiftMultiply64x256,
	Mode::BandedShiftMultiply32x256,
	Mode::BandedShiftMultiply64x1024_2,
	Mode::BandedShiftMultiply32x1024_2,
	Mode::BandedShiftMultiply64x512_2,
	Mode::BandedShiftMultiply32x512_2,
	Mode::BandedShiftMultiply64x256_2,
	Mode::BandedShiftMultiply32x256_2,
	Mode::BandedTensorMultiply64x512,
	Mode::BandedTensorMultiply32x128,
	Mode::BandedTensorMultiply16x32
};

static Mode StandardMatrixMultiplyTests[] = {
	Mode::Naive,
	Mode::GlobalShared64x1024,
	Mode::GlobalShared64x512,
	Mode::GlobalShared64x256,
	Mode::GlobalShared76x304,
	Mode::GlobalShared76x152,
	/*Mode::TextureShared64x1024,
	Mode::TextureShared64x512,
	Mode::TextureShared64x256,*/
	Mode::SharedMemSample1,
	Mode::SharedMemSample2_coop16,
	Mode::SharedMemSample2_coop32,
	Mode::TensorCores,
	Mode::TensorCoresShared64x512,
	Mode::TensorCoresShared32x128,
	Mode::TensorCoresShared16x32,
	Mode::StrassenShared64,
	Mode::StrassenShared32,
	Mode::StrassenSharedNoTemp64,
	Mode::StrassenSharedNoTemp32,
	Mode::StrassenSharedShuffle64,
	Mode::StrassenSharedShuffle32,
	Mode::StrassenShared64_2,
	Mode::StrassenShared32_2,
	Mode::StrassenSharedNoTemp64_2,
	Mode::StrassenSharedNoTemp32_2,
	Mode::StrassenSharedShuffle64_2,
	Mode::StrassenSharedShuffle32_2,
};

void TestWapper(const int deviceIndex, const int iterations, BMResult* results, int testSet)
{
	std::cout << "Starting experiments on device " << deviceIndex << ", iterations: " << iterations << std::endl;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cux(cudaSetDevice(deviceIndex));

	if (testSet == 0)
	{
		BandedMultiplyTest<float> test;
		for (auto mode : BandedMatrixMultiplyTests)
			test.RunTests(deviceIndex, iterations, mode, false, cudaStreamDefault, *results);
	}
	else
	{
		MultiplyTest<float> test;
		for (auto mode : StandardMatrixMultiplyTests)
			test.RunTests(deviceIndex, iterations, mode, false, cudaStreamDefault, *results);
	}
}

int main(int argc, char *argv[])
{
	int testSet = 1;
	BMResult results;
//#ifdef _DEBUG
//	TestWapper(0, 1, &results, testSet);
//#else
	if (argc > 1)
	{
		if (argv[1][1] == 'p' /*profile*/)
		{
			TestWapper(0, 1, &results, testSet);
		}
		else if (argv[1][1] == 'b' /*run benchmark*/)
		{
			int timeToRun = 10; // seconds
			if (argc > 2)
				timeToRun = std::stoi(argv[2]);

			testSet = 0;

			// Create streams (we need 2 streams so that we enqueue workload to one
			// stream (CPU activity) while the GPU is busy with work from the other stream).
			std::vector<cudaStream_t> streams(2);
			for (auto&& stream : streams)
			{
				cux(cudaStreamCreate(&stream));
			}

			BandedMultiplyTest<float> test;
			for (auto mode : BandedMatrixMultiplyTests)
			{
				std::cout << "\nRunning benchmark for experiment: " << ModeName[static_cast<int>(mode)];
				const auto start = std::chrono::high_resolution_clock::now();
				auto end = std::chrono::high_resolution_clock::now();
				int total_iterations = 0;

				const int batchSize = 2000;
				bool graphCreated = false;
				cudaGraph_t graph;
				cudaGraphExec_t instance;
				int i = 0;
				while ((end - start).count() / 1000 / 1000 < timeToRun * 1000)
				{
					if (!graphCreated)
					{
						cux(cudaStreamBeginCapture(streams[i], cudaStreamCaptureModeGlobal));

						test.RunTests(0, batchSize, mode, true, streams[i], results);

						cux(cudaStreamEndCapture(streams[i], &graph));
						cux(cudaGraphInstantiate(&instance, graph, NULL, NULL, 0));
						graphCreated = true;
					}

					cux(cudaStreamSynchronize(streams[i]));
					cux(cudaGraphLaunch(instance, streams[i]));

					total_iterations += batchSize;
					end = std::chrono::high_resolution_clock::now();
					std::cout << ".";
					i = (i + 1) % streams.size();
				}

				cux(cudaDeviceSynchronize());
				end = std::chrono::high_resolution_clock::now();

				const auto elapsedTimeInMicroSec = (end - start).count() / 1000;
				const long double elapsedTimeInSec = elapsedTimeInMicroSec / 1000.0 / 1000.0;
				const long double flops = test.GetFloatingOperations() / elapsedTimeInSec * total_iterations;
				std::cout << "\nTotal iterations: " << total_iterations << ", Total time elapsed (ms): " << elapsedTimeInMicroSec / 1000
					<< ", floating-point performance (GigaFLOPS): " << flops / 1000.0 / 1000.0 / 1000.0 << std::endl;
			}

			// Destroy streams
			for (auto&& stream : streams)
			{
				cux(cudaStreamDestroy(stream));
			}
		}
	}
	else
	{
		int devices;
		cux(cudaGetDeviceCount(&devices));

		std::vector<std::thread> threads;
		for (int d = 0; d < devices; ++d)
		{
			threads.emplace_back(TestWapper, d, 3, &results, testSet);
		}

		for (auto&& t : threads)
		{
			t.join();
		}

		std::vector<Result> rank;
		std::cout << "\nResults:";
		for (auto&& r : results.timings)
		{
			std::cout << std::endl << ModeName[(int)r.first] << ": ";
			std::vector<long long> runTimes;
			std::vector<unsigned long long int> kernelTimers[Timer::count];
			for (auto&& result : r.second)
			{
				runTimes.push_back(result.testRunTime);
				std::cout << result.testRunTime << ", ";

				// Process the kernel timers
				for (int timer = 0; timer < Timer::count; ++timer)
				{
					kernelTimers[timer].push_back(result.cuda_timers__H[timer]);
				}
			}

			const long long avg = average(runTimes);
			const double sDev = std_Dev(runTimes);

			Result avgRes{ r.first, avg, r.second[0].grid_size };

			std::cout << "Mean: " << avg << "us; Std. Dev.: " << sDev;
			for (int timer = 0; timer < Timer::count; ++timer)
			{
				avgRes.cuda_timers__H[timer] = average(kernelTimers[timer]);
			}

			if (rank.size() == 0)
				rank.push_back(avgRes);
			else
			{
				auto it = rank.begin();
				while (it != rank.end() && it->testRunTime < avg)
					++it;

				rank.insert(it, avgRes);
			}
		}

		auto itLongest = std::max_element(ModeName.begin(), ModeName.end(), [](auto&& a, auto&& b) { return a.size() < b.size(); });
		std::cout << "\n\nRanking:\n";
		for (auto&& r : rank)
		{
			std::cout << std::endl << std::setw(itLongest->size()) << ModeName[(int)r.mode] << ": " << r.testRunTime << "us. Grid size: " << r.grid_size << ". ";
			std::cout << "Kernel timers: ";
			for (int timer = 0; timer < Timer::count; ++timer)
			{
				std::cout << TimerName[timer] << ": ";
				/*for (auto&& reading : kernelTimers[timer])
					std::cout << reading << ", ";*/
				std::cout << "Avg: " << r.cuda_timers__H[timer] << " ticks. ";
			}
		}
	}
//#endif

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cux(cudaDeviceReset());

	return 0;
}
