#pragma once

#include <iostream>
#include <time.h>
#include <random>
#include <cuda_runtime.h>
#include <cuda_fp16.h>

template<typename _Ty>
__host__ __device__ __forceinline__ _Ty getVal(const _Ty* mat, const int r, const int c, const int W)
{
	return mat[r * W + c];
}

template<int W, typename _Ty>
__device__ __forceinline__ _Ty getVal(const _Ty* mat, const int r, const int c)
{
	return mat[r * W + c];
}

template<typename _Ty>
__device__ __forceinline__ void setVal(_Ty* mat, const int r, const int c, const int W, const _Ty v)
{
	mat[r * W + c] = v;
}

template<int W, typename _Ty>
__device__ __forceinline__ void setVal(_Ty* mat, const int r, const int c, const _Ty v)
{
	mat[r * W + c] = v;
}

template<typename _Ty>
class Matrix
{
public:
	Matrix(const int height, const int width, bool clear);
	~Matrix();
	Matrix& operator=(const Matrix& other) noexcept;

	_Ty* getHostPtr() const { return mat; }
    _Ty* getDevicePtr() const { return mat__D; }
	int height() const { return H; }
	int width() const { return W; }

	void reset();

	double operator==(Matrix& other);

	//void multiply(const Matrix& other, Matrix& result) const;

	void print();

private:
	void HydrateFromDevice();

	double compare(Matrix& other) const;

private:
	_Ty* mat = nullptr;
	_Ty* mat__D = nullptr;
    const int W;
    const int H;
};
