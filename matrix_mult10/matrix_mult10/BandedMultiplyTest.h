#pragma once

#include "TestBase.h"
#include <cuda_pipeline.h>
#include <cuda_texture_types.h>

#define N0 1024
#define N1 1024
#define N2 1024

using namespace nvcuda::experimental;

template<typename _Ty>
class BandedMultiplyTest final : public TestBase
{
public:
	void RunTests(const int deviceIndex, const int iterations, const Mode test, const bool benchmark, const cudaStream_t stream, BMResult& results);
	size_t GetFloatingOperations() const { return 2ULL * N0 * N1 * N2; }

	BandedMultiplyTest()
		: t0(N0, N1, true)
		, t1(N0, N2, false)
		, t2(N0 + N2, N1, false) // the actual no. of rows N0 + N2 - 1, we intentionally added an extra empty
								 // row so that it becomes a multiple of 64 and the kernel doesn't have to check for bounds
		, baseline(N0, N1, true)
	{
	}

private:
	Matrix<_Ty> t0;
	Matrix<_Ty> t1;
	Matrix<_Ty> t2;
	Matrix<_Ty> baseline;
};

template<int BLOCK_SIZE, int TILE_DIM, int STRIDE, int TILE, int NTILES, typename _Ty>
__device__ __forceinline__ void CopyMatrixDataToSharedMem(
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	_Ty* __restrict__ rowsT1,
	_Ty* __restrict__ colsT2,
	pipeline& pipe)
{
	const int blockOffsetY = blockIdx.y * TILE_DIM;
	const int tileCol = threadIdx.x % TILE_DIM;
	const int t2Col = blockIdx.x * TILE_DIM + tileCol;
	const int tileOffset = (blockIdx.y + TILE) * TILE_DIM;
	const int col = tileOffset + tileCol;
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int tileRow = p / TILE_DIM;
		const int t1Row = blockOffsetY + tileRow;
		const int t1Col = col - t1Row;
		if (TILE == 0 || TILE == NTILES - 1)	// only the first and the last tile need to check for bounds
		{
			if (t1Col < 0 || t1Col >= N2)
				rowsT1[p] = 0;
			else
				memcpy_async(rowsT1[p], t1[t1Row * N2 + t1Col], pipe);
		}
		else
		{
			memcpy_async(rowsT1[p], t1[t1Row * N2 + t1Col], pipe);
		}

		const int t2Row = tileOffset + tileRow;
		memcpy_async(colsT2[p], t2[t2Row * N1 + t2Col], pipe);
	}

	pipe.commit_and_wait();
}

template<int BLOCK_SIZE, int TILE_DIM, int STRIDE, int TILE, int NTILES, typename _Ty, typename _Uy>
__device__ __forceinline__ void CopyMatrixDataToSharedMem(
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	_Uy* __restrict__ rowsT1,
	_Uy* __restrict__ colsT2,
	pipeline&)
{
	const int blockOffsetY = blockIdx.y * TILE_DIM;
	const int tileCol = threadIdx.x % TILE_DIM;
	const int t2Col = blockIdx.x * TILE_DIM + tileCol;
	const int tileOffset = (blockIdx.y + TILE) * TILE_DIM;
	const int col = tileOffset + tileCol;
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int tileRow = p / TILE_DIM;
		const int t1Row = blockOffsetY + tileRow;
		const int t1Col = col - t1Row;
		if (TILE == 0 || TILE == NTILES - 1)	// only the first and the last tile need to check for bounds
		{
			rowsT1[p] = (t1Col >= 0 && t1Col < N2) ? getVal<N2>(t1, t1Row, t1Col) : 0;
		}
		else
		{
			rowsT1[p] = getVal<N2>(t1, t1Row, t1Col);
		}

		const int t2Row = tileOffset + tileRow;
		colsT2[p] = getVal<N1>(t2, t2Row, t2Col);
	}
}

template<int BLOCK_SIZE, int TILE_DIM, int STRIDE, typename _Ty, typename _Uy>
__device__ __forceinline__ void CopyMatrixDataToSharedMem(
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	_Uy* __restrict__ rowsT1,
	_Uy* __restrict__ colsT2,
	const int tile,
	pipeline&)
{
	const int blockOffsetY = blockIdx.y * TILE_DIM;
	const int tileCol = threadIdx.x % TILE_DIM;
	const int t2Col = blockIdx.x * TILE_DIM + tileCol;
	const int tileOffset = (blockIdx.y + tile) * TILE_DIM;
	const int col = tileOffset + tileCol;
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int tileRow = p / TILE_DIM;
		const int t1Row = blockOffsetY + tileRow;
		const int t1Col = col - t1Row;
		rowsT1[p] = (t1Col >= 0 && t1Col < N2) ? getVal<N2>(t1, t1Row, t1Col) : 0;

		const int t2Row = tileOffset + tileRow;
		colsT2[p] = getVal<N1>(t2, t2Row, t2Col);

		// Print to verify we have coalesced global memory access and
		// no bank conflicts when writing to shared mem
		/*if (blockIdx.x == 0 && blockIdx.y == 0 && threadIdx.x < 32)
		{
			printf("%d (%d): %d %d\n", threadIdx.x, p, t1Row * N2 + t1Col, t2Row * N1 + t2Col);
		}*/
	}
}