﻿#include "MultiplyTest.h"

template<typename _Ty>
template<bool USE_TEXTURE, int TILE_DIM, int BLOCK_SIZE, int WS, int STRIDE>
__device__ void MultiplyTest<_Ty>::CalcAllTiles(
	const int tid,
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	_Ty* __restrict__ rowsA, const int nTiles,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	_Ty sum[STRIDE] = {};

	const int rc = blockIdx.y * TILE_DIM;
	const int cc = blockIdx.x * TILE_DIM;
	const int tOffset = tid % TILE_DIM;
	const int x = cc + tOffset;

	_Ty* __restrict__ colsB = rowsA + WS;
	for (int tile = 0; tile < nTiles; ++tile)
	{
		TIMER_TIC(data_copy);

		// 1. copy the data required for this tile from global --> shared memory
		CopyMatrixDataToSharedMem<USE_TEXTURE, BLOCK_SIZE, TILE_DIM, STRIDE>(a, b, rowsA, colsB, wa, wb, tile);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// 2. Calculate the values for this tile
		MultiplyTile<TILE_DIM, BLOCK_SIZE>(tid, rowsA, colsB, sum);

		TIMER_TOC(compute);

		if (tile < nTiles - 1)
			__syncthreads();
	}

	TIMER_TIC(result_copy);

	// 3. copy result in shared mem to global memory
	for (int i = 0, p = tid; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int row = rc + (p / TILE_DIM);
		const _Ty val = sum[i];
#ifdef _DEBUG
		if (std::is_integral<_Ty>::value)
		{
			const _Ty v = getVal(baseline, row, x, wb);
			assert(val == v);
		}
#endif
		setVal(c, row, x, wb, val);
	}

	TIMER_TOC(result_copy);
}

template<typename _Ty, int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS, int WS = TILE_DIM * TILE_DIM>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
multiplySharedMemTuple(
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	const int nTiles,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	__shared__ _Ty rolcolBuf[2 * WS];
	MultiplyTest<_Ty>::CalcAllTiles<false, TILE_DIM, BLOCK_SIZE>(threadIdx.x, a, wa, b, wb, rolcolBuf, nTiles,
#ifdef _DEBUG
		baseline,
#endif
		c);
}

template<typename _Ty, int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS, int WS = TILE_DIM * TILE_DIM>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
multiplyTextureShared(const int wb, const int nTiles,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	__shared__ _Ty rolcolBuf[2 * WS]; // TILE_DIM * TILE_DIM for each a, b
	MultiplyTest<_Ty>::CalcAllTiles<true, TILE_DIM, BLOCK_SIZE>(threadIdx.x, nullptr, 0, nullptr, wb, rolcolBuf, nTiles,
#ifdef _DEBUG
		baseline,
#endif
		c);
}

template<typename _Ty>
template<int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS_OVERRIDE, int SHAREDMEM_PER_BLOCK>
int MultiplyTest<_Ty>::GlobalSharedKernelHelper(AutoTimer& timer, const int deviceIndex)
{
	const auto& [minBlocks, reqSharedMemPercent] = CalcMinBlocks<MIN_BLOCKS_OVERRIDE, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>(deviceIndex);
	const dim3 gridDim(ceilDiv(c.width(), TILE_DIM), ceilDiv(c.height(), TILE_DIM));
	const int nTiles = ceilDiv(a.width(), TILE_DIM);
	timer.reset();
	switch (minBlocks)
	{
	case 1:
		multiplySharedMemTuple<_Ty, TILE_DIM, BLOCK_SIZE, 1><<<gridDim, BLOCK_SIZE>>>(
			a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;

	case 2:
		multiplySharedMemTuple<_Ty, TILE_DIM, BLOCK_SIZE, 2><<<gridDim, BLOCK_SIZE>>>(
			a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;

	default:
		std::cout << "No appropriate kernel found (please add one). Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << minBlocks << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
		return -1;
	}

	return gridDim.x * gridDim.y;
}

template int MultiplyTest<float>::GlobalSharedKernelHelper<64, 1024>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::GlobalSharedKernelHelper<64, 512>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::GlobalSharedKernelHelper<64, 256>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::GlobalSharedKernelHelper<76, 152>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::GlobalSharedKernelHelper<76, 304>(AutoTimer& timer, const int deviceIndex);

template<typename _Ty>
template<int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS_OVERRIDE, int SHAREDMEM_PER_BLOCK>
int MultiplyTest<_Ty>::TextureSharedKernelHelper(AutoTimer& timer, const int deviceIndex)
{
	const auto& [minBlocks, reqSharedMemPercent] = CalcMinBlocks<MIN_BLOCKS_OVERRIDE, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>(deviceIndex);
	const dim3 gridDim(ceilDiv(c.width(), TILE_DIM), ceilDiv(c.height(), TILE_DIM));
	const int nTiles = ceilDiv(a.width(), TILE_DIM);
	cux(cudaBindTexture2D(NULL, texA2, a.getDevicePtr(), cudaCreateChannelDesc<_Ty>(), a.width(), a.height(), a.width() * sizeof(_Ty)));
	cux(cudaBindTexture2D(NULL, texB2, b.getDevicePtr(), cudaCreateChannelDesc<_Ty>(), b.width(), b.height(), b.width() * sizeof(_Ty)));
	timer.reset();
	switch (minBlocks)
	{
	case 1:
		multiplyTextureShared<_Ty, TILE_DIM, BLOCK_SIZE, 1><<<gridDim, BLOCK_SIZE>>>(
			b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;

	case 2:
		multiplyTextureShared<_Ty, TILE_DIM, BLOCK_SIZE, 2><<<gridDim, BLOCK_SIZE>>>(
			b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;

	default:
		std::cout << "No appropriate kernel found (please add one). Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << minBlocks << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
		return -1;
	}

	return gridDim.x * gridDim.y;
}

template int MultiplyTest<float>::TextureSharedKernelHelper<64, 1024, 2>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::TextureSharedKernelHelper<64, 512, 2>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::TextureSharedKernelHelper<64, 256, 2>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::TextureSharedKernelHelper<64, 256, 3>(AutoTimer& timer, const int deviceIndex);