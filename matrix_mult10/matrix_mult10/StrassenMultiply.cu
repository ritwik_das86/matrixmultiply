﻿#include "MultiplyTest.h"

template<int TILE_DIM, int BLOCK_SIZE, typename _Ty, typename _Uy, int SUBTILE_DIM = TILE_DIM / 2>
__device__ void MultiplyStrassen(
	const int tid,
	const _Uy* __restrict__ rowsA,
	const _Uy* __restrict__ colsB,
	_Uy* __restrict__ t0,
	_Uy* __restrict__ t1,
	_Ty* __restrict__ sum)
{
	// TILE_DIM == 64, 32
	// BLOCK_SIZE == 1024, 256
	const int r = tid / SUBTILE_DIM;
	const int c = tid % SUBTILE_DIM;

	const _Uy a11 = getVal<TILE_DIM>(rowsA, r, c);
	const _Uy a12 = getVal<TILE_DIM>(rowsA, r, c + SUBTILE_DIM);
	const _Uy a21 = getVal<TILE_DIM>(rowsA, r + SUBTILE_DIM, c);
	const _Uy a22 = getVal<TILE_DIM>(rowsA, r + SUBTILE_DIM, c + SUBTILE_DIM);
	const _Uy b11 = getVal<TILE_DIM>(colsB, r, c);
	const _Uy b12 = getVal<TILE_DIM>(colsB, r, c + SUBTILE_DIM);
	const _Uy b21 = getVal<TILE_DIM>(colsB, r + SUBTILE_DIM, c);
	const _Uy b22 = getVal<TILE_DIM>(colsB, r + SUBTILE_DIM, c + SUBTILE_DIM);

	// 2 temp slots (2 * 32 * 32 * sizeof(float) = 8k)
	// M1 = (a11 + a22).(b11 + b22)
	// M2 = (a21 + a22).B11
	// M3 = A11.(b12 - b22)
	// M4 = A22.(b21 - b11)
	// M5 = (a11 + a12).B22
	// M6 = (a21 - a11).(b11 + b12)
	// M7 = (a12 - a22).(b21 + b22)

	// C11 = M1 + M4 - M5 + M7
	// C12 = M3 + M5
	// C21 = M2 + M4
	// C22 = M1 - M2 + M3 + M6

	// [M1, M4, M5, M7] --> C11
	t0[tid] = a11 + a22;
	t1[tid] = b11 + b22;
	__syncthreads();

	//	m1 = t0.t1 (M1)
	_Ty m1 = calculateCellValue<0, 0, SUBTILE_DIM>(SUBTILE_DIM, t0, SUBTILE_DIM, t1, SUBTILE_DIM, r, c);

	__syncwarp();
	t0[tid] = b21 - b11;
	__syncthreads();

	//	m2 = A22.t0 (M4)
	_Ty m2 = calculateCellValue<SUBTILE_DIM, 0, SUBTILE_DIM>(SUBTILE_DIM, rowsA, TILE_DIM, t0, SUBTILE_DIM, r + SUBTILE_DIM, c);

	t1[tid] = a11 + a12;

	//	m3 = t1.B22 (M5)
	__syncwarp();
	_Ty m3 = calculateCellValue<0, SUBTILE_DIM, SUBTILE_DIM>(SUBTILE_DIM, t1, SUBTILE_DIM, colsB, TILE_DIM, r, c + SUBTILE_DIM);

	__syncwarp();
	t1[tid] = b21 + b22;
	__syncthreads();
	t0[tid] = a12 - a22;
	__syncwarp();

	//	m4 = t0.t1 (M7)
	_Ty m4 = calculateCellValue<0, 0, SUBTILE_DIM>(SUBTILE_DIM, t0, SUBTILE_DIM, t1, SUBTILE_DIM, r, c);

	// C11 = m1 + m2 - m3 + m4
	sum[0] += m1 + m2 - m3 + m4;

	// [M1, M4, M5, M3] --> C12
	__syncwarp();
	t0[tid] = b12 - b22;
	__syncthreads();

	//	m4 = A11.t0 (M3)
	m4 = calculateCellValue<0, 0, SUBTILE_DIM>(SUBTILE_DIM, rowsA, TILE_DIM, t0, SUBTILE_DIM, r, c);

	// C12 = m4 + m3
	sum[1] += m4 + m3;

	// [M1, M4, M2, M3] --> C21
	t1[tid] = a21 + a22;

	//	m3 = t1.B11 (M2)
	__syncwarp();
	m3 = calculateCellValue<0, 0, SUBTILE_DIM>(SUBTILE_DIM, t1, SUBTILE_DIM, colsB, TILE_DIM, r, c);

	// C21 = m3 + m2
	sum[2] += m3 + m2;

	// [M1, M6, M2, M3] --> C22
	__syncwarp();
	t1[tid] = b11 + b12;
	__syncthreads();
	t0[tid] = a21 - a11;
	__syncwarp();

	//	m2 = t0.t1 (M6)
	m2 = calculateCellValue<0, 0, SUBTILE_DIM>(SUBTILE_DIM, t0, SUBTILE_DIM, t1, SUBTILE_DIM, r, c);

	// C22 = m1 - m3 + m4 + m2
	sum[3] += m1 - m3 + m4 + m2;
}

template<int TILE_DIM, int BLOCK_SIZE, typename _Ty, typename _Uy, int SUBTILE_DIM = TILE_DIM / 2>
__device__ void MultiplyStrassenNoTemp(
	const int tid,
	const _Uy* __restrict__ rowsA,
	const _Uy* __restrict__ colsB,
	_Ty* __restrict__ sum)
{
	// TILE_DIM == 64, 32
	// BLOCK_SIZE == 1024, 256
	const int r = tid / SUBTILE_DIM;
	const int c = tid % SUBTILE_DIM;

	// 2 temp slots (2 * 32 * 32 * sizeof(float) = 8k)
	// M1 = (a11 + a22).(b11 + b22)
	// M2 = (a21 + a22).B11
	// M3 = A11.(b12 - b22)
	// M4 = A22.(b21 - b11)
	// M5 = (a11 + a12).B22
	// M6 = (a21 - a11).(b11 + b12)
	// M7 = (a12 - a22).(b21 + b22)

	_Ty m1{};
	_Ty m2{};
	_Ty m3{};
	_Ty m4{};
	_Ty m5{};
	_Ty m6{};
	_Ty m7{};
	for (int k = 0; k < SUBTILE_DIM; ++k)
	{
		const _Uy a11 = getVal<TILE_DIM>(rowsA, r, k);
		const _Uy a12 = getVal<TILE_DIM>(rowsA, r, k + SUBTILE_DIM);
		const _Uy a21 = getVal<TILE_DIM>(rowsA, r + SUBTILE_DIM, k);
		const _Uy a22 = getVal<TILE_DIM>(rowsA, r + SUBTILE_DIM, k + SUBTILE_DIM);
		const _Uy b11 = getVal<TILE_DIM>(colsB, k, c);
		const _Uy b12 = getVal<TILE_DIM>(colsB, k, c + SUBTILE_DIM);
		const _Uy b21 = getVal<TILE_DIM>(colsB, k + SUBTILE_DIM, c);
		const _Uy b22 = getVal<TILE_DIM>(colsB, k + SUBTILE_DIM, c + SUBTILE_DIM);

		m1 = ma(a11 + a22, b11 + b22, m1);
		m2 = ma(a21 + a22, b11, m2);
		m3 = ma(a11, b12 - b22, m3);
		m4 = ma(a22, b21 - b11, m4);
		m5 = ma(a11 + a12, b22, m5);
		m6 = ma(a21 - a11, b11 + b12, m6);
		m7 = ma(a12 - a22, b21 + b22, m7);
	}

	sum[0] += m1 + m4 - m5 + m7;
	sum[1] += m3 + m5;
	sum[2] += m2 + m4;
	sum[3] += m1 - m2 + m3 + m6;
}

template<int TILE_DIM, int BLOCK_SIZE, typename _Ty, typename _Uy, int SUBTILE_DIM = TILE_DIM / 2>
__device__ void MultiplyStrassenShuffle(
	const int tid,
	const _Ty* __restrict__ rowsA, const int wa,
	const _Uy* __restrict__ colsB,
	_Ty* __restrict__ sum)
{
	// TILE_DIM == 64, 32
	// BLOCK_SIZE == 1024, 256
	const int r = tid / SUBTILE_DIM;
	const int c = tid % SUBTILE_DIM;

	unsigned mask = 0;
	if (SUBTILE_DIM == warpSize)
	{
		mask = 0xffffffff;
	}
	else if (SUBTILE_DIM == warpSize / 2)
	{
		if (tid % warpSize < warpSize / 2)
		{
			mask = 0xffff;
		}
		else
		{
			mask = 0xffff0000;
		}
	}

	// Only either rows or columns can be accessed using shuffle intrinsics,
	// we choose to do rows here simce it is simpler to calculate their indices.
	const _Uy a11 = getVal(rowsA, r, c, wa);
	const _Uy a12 = getVal(rowsA, r, c + SUBTILE_DIM, wa);
	const _Uy a21 = getVal(rowsA, r + SUBTILE_DIM, c, wa);
	const _Uy a22 = getVal(rowsA, r + SUBTILE_DIM, c + SUBTILE_DIM, wa);

	const _Uy P = a11 + a22;
	const _Uy R = a21 + a22;
	const _Uy U = a11 + a12;
	const _Uy V = a21 - a11;
	const _Uy X = a12 - a22;

	// C11 = M1 + M4 - M5 + M7
	// C12 = M3 + M5
	// C21 = M2 + M4
	// C22 = M1 - M2 + M3 + M6

	//	M1 = (a11 + a22).(b11 + b22) = P.(b11 + b22)
	//	M2 = (a21 + a22).B11 = R.B11
	//	M3 = A11.(b12 - b22)
	//	M4 = A22.(b21 - b11)
	//	M5 = (a11 + a12).B22 = U.B22
	//	M6 = (a21 - a11).(b11 + b12) = V.(b11 + b12)
	//	M7 = (a12 - a22).(b21 + b22) = X.(b21 + b22)
	_Ty m1{};
	_Ty m2{};
	_Ty m3{};
	_Ty m4{};
	_Ty m5{};
	_Ty m6{};
	_Ty m7{};
	for (int k = 0; k < SUBTILE_DIM; ++k)
	{
		const _Uy b11 = getVal<TILE_DIM>(colsB, k, c);
		const _Uy b12 = getVal<TILE_DIM>(colsB, k, c + SUBTILE_DIM);
		const _Uy b21 = getVal<TILE_DIM>(colsB, k + SUBTILE_DIM, c);
		const _Uy b22 = getVal<TILE_DIM>(colsB, k + SUBTILE_DIM, c + SUBTILE_DIM);

		m1 = ma(__shfl_sync(mask, P, k, SUBTILE_DIM), b11 + b22, m1);
		m2 = ma(__shfl_sync(mask, R, k, SUBTILE_DIM), b11, m2);
		m3 = ma(__shfl_sync(mask, a11, k, SUBTILE_DIM), b12 - b22, m3);
		m4 = ma(__shfl_sync(mask, a22, k, SUBTILE_DIM), b21 - b11, m4);
		m5 = ma(__shfl_sync(mask, U, k, SUBTILE_DIM), b22, m5);
		m6 = ma(__shfl_sync(mask, V, k, SUBTILE_DIM), b11 + b12, m6);
		m7 = ma(__shfl_sync(mask, X, k, SUBTILE_DIM), b21 + b22, m7);
	}

	sum[0] += m1 + m4 - m5 + m7;
	sum[1] += m3 + m5;
	sum[2] += m2 + m4;
	sum[3] += m1 - m2 + m3 + m6;
}

template<int TILE_DIM, typename _Ty, int SUBTILE_DIM = TILE_DIM / 2>
__device__ __forceinline__ void CopyResult(
	const int wb,
	const _Ty* sum,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	TIMER_TIC(result_copy);

	const int rc = (blockIdx.y * TILE_DIM) + (threadIdx.x / SUBTILE_DIM);
	const int cc = (blockIdx.x * TILE_DIM) + (threadIdx.x % SUBTILE_DIM);
#ifdef _DEBUG
	const _Ty c11 = getVal(baseline, rc, cc, wb);
	const _Ty c12 = getVal(baseline, rc, cc + SUBTILE_DIM, wb);
	const _Ty c21 = getVal(baseline, rc + SUBTILE_DIM, cc, wb);
	const _Ty c22 = getVal(baseline, rc + SUBTILE_DIM, cc + SUBTILE_DIM, wb);
	if (std::is_integral<_Ty>::value)
	{
		assert(sum[0] == c11);
		assert(sum[1] == c12);
		assert(sum[2] == c21);
		assert(sum[3] == c22);
	}
#endif
	setVal(c, rc, cc, wb, sum[0]);
	setVal(c, rc, cc + SUBTILE_DIM, wb, sum[1]);
	setVal(c, rc + SUBTILE_DIM, cc, wb, sum[2]);
	setVal(c, rc + SUBTILE_DIM, cc + SUBTILE_DIM, wb, sum[3]);

	TIMER_TOC(result_copy);
}

template<typename _Ty, typename _Uy, int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS, int SUBTILE_DIM = TILE_DIM / 2, int WS = TILE_DIM * TILE_DIM>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
multiplySharedMemTupleStrassen(
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	const int nTiles,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	extern __shared__ _Ty buf[];
	_Ty sum[4] = {};

	_Uy* __restrict__ colsB = reinterpret_cast<_Uy*>(buf) + WS;
	_Uy* __restrict__ t0 = colsB + WS;
	_Uy* __restrict__ t1 = t0 + (SUBTILE_DIM * SUBTILE_DIM);

	for (int tile = 0; tile < nTiles; ++tile)
	{
		TIMER_TIC(data_copy);

		// 1. copy the data required for this tile from global --> shared memory
		MultiplyTest<_Ty>::CopyMatrixDataToSharedMem<false, BLOCK_SIZE, TILE_DIM, 4>(a, b, reinterpret_cast<_Uy*>(buf), colsB, wa, wb, tile);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// 2. Calculate the values for this tile
		MultiplyStrassen<TILE_DIM, BLOCK_SIZE>(threadIdx.x, reinterpret_cast<_Uy*>(buf), colsB, t0, t1, sum);

		TIMER_TOC(compute);
	}

	CopyResult<TILE_DIM>(wb, sum,
#ifdef _DEBUG
		baseline,
#endif
		c);
}

template<typename _Ty, typename _Uy, int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS, int SUBTILE_DIM = TILE_DIM / 2, int WS = TILE_DIM * TILE_DIM>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
multiplySharedMemTupleStrassenNoTemp(
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	const int nTiles,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	extern __shared__ _Ty buf[];
	_Ty sum[4] = {};

	_Uy* __restrict__ colsB = reinterpret_cast<_Uy*>(buf) + WS;

	for (int tile = 0; tile < nTiles; ++tile)
	{
		TIMER_TIC(data_copy);

		// 1. copy the data required for this tile from global --> shared memory
		MultiplyTest<_Ty>::CopyMatrixDataToSharedMem<false, BLOCK_SIZE, TILE_DIM, 4>(a, b, reinterpret_cast<_Uy*>(buf), colsB, wa, wb, tile);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// 2. Calculate the values for this tile
		MultiplyStrassenNoTemp<TILE_DIM, BLOCK_SIZE>(threadIdx.x, reinterpret_cast<_Uy*>(buf), colsB, sum);

		if (tile < nTiles - 1)
			__syncthreads();

		TIMER_TOC(compute);
	}

	CopyResult<TILE_DIM>(wb, sum,
#ifdef _DEBUG
		baseline,
#endif
		c);
}

template<typename _Ty, typename _Uy, int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS, int SUBTILE_DIM = TILE_DIM / 2, int WS = TILE_DIM * TILE_DIM>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
multiplySharedMemTupleStrassenShuffle(
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	const int nTiles,
#ifdef _DEBUG
	const _Ty* __restrict__ baseline,
#endif
	_Ty* __restrict__ c)
{
	extern __shared__ _Ty buf[];
	_Ty sum[4] = {};

	_Uy* __restrict__ colsB = reinterpret_cast<_Uy*>(buf);

	for (int tile = 0; tile < nTiles; ++tile)
	{
		TIMER_TIC(data_copy);

		// 1. copy the data required for this tile from global --> shared memory
		MultiplyTest<_Ty>::CopyMatrixDataToSharedMem<false, BLOCK_SIZE, TILE_DIM, 4>(b, colsB, wb, tile);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// 2. Calculate the values for this tile
		const _Ty* __restrict__ rows = a + (blockIdx.y * TILE_DIM * wa) + tile * TILE_DIM;
		MultiplyStrassenShuffle<TILE_DIM, BLOCK_SIZE>(threadIdx.x, rows, wa, colsB, sum);

		if (tile < nTiles - 1)
			__syncthreads();

		TIMER_TOC(compute);
	}

	CopyResult<TILE_DIM>(wb, sum,
#ifdef _DEBUG
		baseline,
#endif
		c);
}

template<
	typename _Ty,
	typename _Uy,
	int MODE,
	int TILE_DIM,
	int BLOCK_SIZE,
	int SHAREDMEM_PER_BLOCK,
	int MIN_BLOCKS>
int StrassenMultiplyLauncher(AutoTimer& timer, Matrix<_Ty>& c, const Matrix<_Ty>& a, const Matrix<_Ty>& b, const Matrix<_Ty>& baseline, const int reqSharedMemPercent)
{
	const dim3 gridDim(c.width() / TILE_DIM, c.height() / TILE_DIM);
	const int nTiles = ceilDiv(a.width(), TILE_DIM);
	switch (MODE)
	{
	case 0:
		cux(cudaFuncSetAttribute(multiplySharedMemTupleStrassen<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS>, cudaFuncAttributePreferredSharedMemoryCarveout, reqSharedMemPercent));
		timer.reset();
		multiplySharedMemTupleStrassen<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS><<<gridDim, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>>>(
			a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;

	case 1:
		cux(cudaFuncSetAttribute(multiplySharedMemTupleStrassenNoTemp<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS>, cudaFuncAttributePreferredSharedMemoryCarveout, reqSharedMemPercent));
		timer.reset();
		multiplySharedMemTupleStrassenNoTemp<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS><<<gridDim, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>>>(
			a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;

	case 2:
		cux(cudaFuncSetAttribute(multiplySharedMemTupleStrassenShuffle<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS>, cudaFuncAttributePreferredSharedMemoryCarveout, reqSharedMemPercent));
		timer.reset();
		multiplySharedMemTupleStrassenShuffle<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS><<<gridDim, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>>>(
			a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), nTiles,
#ifdef _DEBUG
			baseline.getDevicePtr(),
#endif
			c.getDevicePtr());
		break;
	}
	return gridDim.x * gridDim.y;
}

template<typename _Ty>
template<
	typename _Uy,
	int MODE,
	int TILE_DIM,
	int MIN_BLOCKS_OVERRIDE,
	int BLOCK_SIZE,
	int SHAREDMEM_PER_BLOCK>
int MultiplyTest<_Ty>::StrassenMultiplyHelper(AutoTimer& timer, const int deviceIndex)
{
	const auto& [minBlocks, reqSharedMemPercent] = CalcMinBlocks<MIN_BLOCKS_OVERRIDE, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>(deviceIndex);

	switch (minBlocks)
	{
	case 1:
		return StrassenMultiplyLauncher<_Ty, _Uy, MODE, TILE_DIM, BLOCK_SIZE, SHAREDMEM_PER_BLOCK, 1>(timer, c, a, b, baseline, reqSharedMemPercent);
	case 4:
		return StrassenMultiplyLauncher<_Ty, _Uy, MODE, TILE_DIM, BLOCK_SIZE, SHAREDMEM_PER_BLOCK, 4>(timer, c, a, b, baseline, reqSharedMemPercent);
	default:
		std::cout << "No appropriate kernel found (please add one). Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << minBlocks << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
		return -1;
	}
}

template int MultiplyTest<float>::StrassenMultiplyHelper<float, 0, 64>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<float, 0, 32>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<float, 1, 64>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<float, 1, 32>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<float, 2, 64>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<float, 2, 32>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<half, 0, 64>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<half, 0, 32>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<half, 1, 64>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<half, 1, 32>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<half, 2, 64>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::StrassenMultiplyHelper<half, 2, 32>(AutoTimer& timer, const int deviceIndex);