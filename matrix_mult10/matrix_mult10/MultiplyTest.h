#pragma once

#include "TestBase.h"
#include "Matrix.h"
#include <cuda_texture_types.h>

template<typename _Ty>
class MultiplyTest final : public TestBase
{
	constexpr static int S = 9728;
public:
	void RunTests(const int deviceIndex, const int iterations, const Mode test, const bool benchmark, const cudaStream_t stream, BMResult& results);
	size_t GetFloatingOperations() const { return 2ULL * a.height() * b.width() * a.width(); }

	MultiplyTest()
		: a(S, S, false)
		, b(S, S, false)
		, c(S, S, true)
		, baseline(S, S, true)
	{
	}

	template<bool USE_TEXTURE, int TILE_DIM, int BLOCK_SIZE, int WS = TILE_DIM * TILE_DIM, int STRIDE = WS / BLOCK_SIZE>
	static __device__ void CalcAllTiles(
		const int tid,
		const _Ty* __restrict__ a, const int wa,
		const _Ty* __restrict__ b, const int wb,
		_Ty* __restrict__ rowsA, const int nTiles,
#ifdef _DEBUG
		const _Ty* __restrict__ baseline,
#endif
		_Ty* __restrict__ c);

	template<bool USE_TEXTURE, int BLOCK_SIZE, int TILE_DIM, int STRIDE, typename _Uy>
	static __device__ __forceinline__ void CopyMatrixDataToSharedMem(
		const _Ty* __restrict__ t1,
		const _Ty* __restrict__ t2,
		_Uy* __restrict__ rowsT1,
		_Uy* __restrict__ colsT2,
		const int wt1,
		const int wt2,
		const int tile);

	template<bool USE_TEXTURE, int BLOCK_SIZE, int TILE_DIM, int STRIDE, typename _Uy>
	static __device__ __forceinline__ void CopyMatrixDataToSharedMem(
		const _Ty* __restrict__ src,
		_Uy* __restrict__ dst,
		const int w,
		const int tile);

	static texture<_Ty, cudaTextureType2D> texA2;
	static texture<_Ty, cudaTextureType2D> texB2;

private:
	Result selector(const Mode mode, const int deviceIndex, const bool benchmark);

	template<
		int TILE_DIM,
		int BLOCK_SIZE,
		int MIN_BLOCKS_OVERRIDE = 32,
		int SHAREDMEM_PER_BLOCK = 2 * TILE_DIM* TILE_DIM * sizeof(_Ty)>
	int TextureSharedKernelHelper(AutoTimer& timer, const int deviceIndex);

	template<
		int TILE_DIM,
		int BLOCK_SIZE,
		int MIN_BLOCKS_OVERRIDE = 32,
		int SHAREDMEM_PER_BLOCK = 2 * TILE_DIM* TILE_DIM * sizeof(_Ty)>
	int GlobalSharedKernelHelper(AutoTimer& timer, const int deviceIndex);

	template<
		int TILE_DIM,
		typename _Uy,
		int MIN_BLOCKS_OVERRIDE = 32,
		int BLOCK_SIZE = TILE_DIM * TILE_DIM / (16 * 16) * 32, // a single warp computes a 16 x 16 tile
		int SHAREDMEM_PER_BLOCK = 2 * TILE_DIM * TILE_DIM * sizeof(_Uy) + TILE_DIM * TILE_DIM * sizeof(_Ty)>
	int TensorMultiplyHelper(AutoTimer& timer, const int deviceIndex);

	int TensorCoresBasic();

	template<
		typename _Uy,
		int MODE,
		int TILE_DIM,
		int MIN_BLOCKS_OVERRIDE = 32,
		int BLOCK_SIZE = TILE_DIM * TILE_DIM / 4, // we partition a tile into 4 subtiles
		int SHAREDMEM_PER_BLOCK = (MODE == 2 ? 1 : 2)* TILE_DIM* TILE_DIM * sizeof(_Uy) + (MODE == 0 ? (2 * BLOCK_SIZE * sizeof(_Uy)) : 0)>
	int StrassenMultiplyHelper(AutoTimer& timer, const int deviceIndex);

	Matrix<_Ty> a;
	Matrix<_Ty> b;
	Matrix<_Ty> c;
	Matrix<_Ty> baseline;
};

template<typename _Ty>
template<bool USE_TEXTURE, int BLOCK_SIZE, int TILE_DIM, int STRIDE, typename _Uy>
__device__ __forceinline__ void MultiplyTest<_Ty>::CopyMatrixDataToSharedMem(
	const _Ty* __restrict__ a,
	const _Ty* __restrict__ b,
	_Uy* __restrict__ rowsA,
	_Uy* __restrict__ colsB,
	const int wa,
	const int wb,
	const int tile)
{
	const int blockOffsetY = blockIdx.y * TILE_DIM;
	const int tileCol = threadIdx.x % TILE_DIM;
	const int bCol = blockIdx.x * TILE_DIM + tileCol;
	const int tileOffset = tile * TILE_DIM;
	const int aCol = tileOffset + tileCol;
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int tileRow = p / TILE_DIM;
		const int aRow = blockOffsetY + tileRow;
		const int bRow = tileOffset + tileRow;

		if (USE_TEXTURE)
		{
			rowsA[p] = tex2D(texA2, aCol, aRow);
			colsB[p] = tex2D(texB2, bCol, bRow);
		}
		else
		{
			rowsA[p] = getVal(a, aRow, aCol, wa);
			colsB[p] = getVal(b, bRow, bCol, wb);
		}
	}
}

template<typename _Ty>
template<bool USE_TEXTURE, int BLOCK_SIZE, int TILE_DIM, int STRIDE, typename _Uy>
__device__ __forceinline__ void MultiplyTest<_Ty>::CopyMatrixDataToSharedMem(
	const _Ty* __restrict__ src,
	_Uy* __restrict__ dst,
	const int w,
	const int tile)
{
	const int bCol = blockIdx.x * TILE_DIM + (threadIdx.x % TILE_DIM);
	const int tileOffset = tile * TILE_DIM;
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int bRow = tileOffset + (p / TILE_DIM);

		if (USE_TEXTURE)
		{
			dst[p] = tex2D(texB2, bCol, bRow);
		}
		else
		{
			dst[p] = getVal(src, bRow, bCol, w);
		}
	}
}

template<int COL_OFFSET_A, int ROW_OFFSET_B, int L = 32, typename _Ty>
__device__ __forceinline__ _Ty calculateCellValue(
	const int D,
	const _Ty* __restrict__ A, const int wa,
	const _Ty* __restrict__ B, const int wb,
	const int r,
	const int c)
{
	_Ty s{};
#pragma unroll(L)
	for (int k = 0; k < D; ++k)
	{
		const _Ty a = getVal(A, r, k + COL_OFFSET_A, wa);
		const _Ty b = getVal(B, k + ROW_OFFSET_B, c, wb);
		s = ma(a, b, s);
	}
	return s;
}