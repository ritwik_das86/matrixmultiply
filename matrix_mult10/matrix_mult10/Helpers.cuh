#include <iostream>
#include <cassert>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cuda_fp16.h>

__host__ __forceinline__ void cux(cudaError_t error)
{
	if (error != cudaSuccess)
	{
		//__debugbreak();
		std::cout << "Error: " << cudaGetErrorName(error) << ", desc: " << cudaGetErrorString(error) << std::endl;
	}
	assert(error == cudaSuccess);
}

template<typename T, typename U>
__host__ __device__ __forceinline__ T ceilDiv(const T a, const U b)
{
	return static_cast<T>(a / b + (a % b > 0));
}

#if defined(__CUDA_ARCH__) && __CUDA_ARCH__ < 530
__device__ __forceinline__ half operator+(const half a, const half b) { return { __half2float(a) + __half2float(b) }; }
__device__ __forceinline__ half operator-(const half a, const half b) { return { __half2float(a) - __half2float(b) }; }
__device__ __forceinline__ half operator*(const half a, const half b) { return { __half2float(a) * __half2float(b) }; }
#endif

template<typename _Ty>
__device__ __forceinline__ _Ty ma(const _Ty a, const _Ty b, const _Ty c) { return c + a * b; }
__device__ __forceinline__ float ma(const half a, const half b, float& c) { return c + __half2float(a * b); }

template<int TILE_DIM, typename _Ty>
__device__ __forceinline__ void SetSum(_Ty* __restrict__ sum, const _Ty* __restrict__ rowsA, const int p, const _Ty colVal, const int k, const int y)
{
	sum[p] += getVal<TILE_DIM>(rowsA, y / TILE_DIM, k) * colVal;
}

template<int TILE_DIM>
__device__ __forceinline__ void SetSum(float* __restrict__ sum, const half* __restrict__ rowsA, const int p, const half colVal, const int k, const int y)
{
	sum[p] += __half2float(getVal<TILE_DIM>(rowsA, y / TILE_DIM, k)) * __half2float(colVal);
}

template<int TILE_DIM, int BLOCK_SIZE, typename _Ty, typename _Uy, int STRIDE = TILE_DIM * TILE_DIM / BLOCK_SIZE>
__device__ __forceinline__ void MultiplyTile(
	const int tid,
	const _Uy* __restrict__ rowsA,
	const _Uy* __restrict__ colsB,
	_Ty* __restrict__ sum)
{
	const int tOffset = tid % TILE_DIM;
	for (int i = 0, qOffset = tid; i < STRIDE; i += 4, qOffset += 4 * BLOCK_SIZE)
	{
		const int iters = std::min(STRIDE - i, 4);
		for (int k = 0; k < TILE_DIM; ++k)
		{
			int y = qOffset;
			const _Ty colVal = getVal<TILE_DIM>(colsB, k, tOffset);
			for (int q = 0; q < iters; ++q)
			{
				SetSum<TILE_DIM>(sum, rowsA, i + q, colVal, k, y);
				y += BLOCK_SIZE;
			}

			// Unrolling is not a good idea
			/*if (iters == 4)
			{
				SetSum<TILE_DIM>(sum, rowsA, i, colVal, k, y);
				y += BLOCK_SIZE;
				SetSum<TILE_DIM>(sum, rowsA, i + 1, colVal, k, y);
				y += BLOCK_SIZE;
				SetSum<TILE_DIM>(sum, rowsA, i + 2, colVal, k, y);
				y += BLOCK_SIZE;
				SetSum<TILE_DIM>(sum, rowsA, i + 3, colVal, k, y);
			}
			else if (iters == 3)
			{
				SetSum<TILE_DIM>(sum, rowsA, i, colVal, k, y);
				y += BLOCK_SIZE;
				SetSum<TILE_DIM>(sum, rowsA, i + 1, colVal, k, y);
				y += BLOCK_SIZE;
				SetSum<TILE_DIM>(sum, rowsA, i + 2, colVal, k, y);
			}
			else if (iters == 2)
			{
				SetSum<TILE_DIM>(sum, rowsA, i, colVal, k, y);
				y += BLOCK_SIZE;
				SetSum<TILE_DIM>(sum, rowsA, i + 1, colVal, k, y);
			}
			else if (iters == 1)
			{
				SetSum<TILE_DIM>(sum, rowsA, i, colVal, k, y);
			}*/
		}
	}
}

template<int MIN_BLOCKS_OVERRIDE, int BLOCK_SIZE, int SHAREDMEM_PER_BLOCK>
__forceinline__ std::pair<int, int> CalcMinBlocks(const int deviceIndex)
{
	cudaDeviceProp prop;
	cux(cudaGetDeviceProperties(&prop, deviceIndex));
	const int maxBlocksBySharedMem = static_cast<int>(prop.sharedMemPerMultiprocessor / SHAREDMEM_PER_BLOCK);
	const int maxBlocks = std::min(prop.maxThreadsPerMultiProcessor / BLOCK_SIZE, prop.maxBlocksPerMultiProcessor);
	const int minBlocks = std::min(std::min(MIN_BLOCKS_OVERRIDE, prop.maxBlocksPerMultiProcessor), std::min(maxBlocksBySharedMem, maxBlocks));
	const int reqSharedMemPercent = static_cast<int>(minBlocks * SHAREDMEM_PER_BLOCK * 100 / prop.sharedMemPerMultiprocessor);
	return { minBlocks, reqSharedMemPercent };
}