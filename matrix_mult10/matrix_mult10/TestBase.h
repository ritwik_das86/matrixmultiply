#pragma once

#include <vector>
#include <array>
#include <unordered_map>
#include <array>
#include <string_view>
#include <mutex>
#include "Matrix.h"
#include "Helpers.cuh"

using namespace std::literals;

//#define USETIMERS

enum class Mode
{
	// Standard matrix-multiply
	Naive,
	SharedMem,
	GlobalShared64x1024,
	GlobalShared64x512,
	GlobalShared64x256,
	GlobalShared76x152,
	GlobalShared76x304,
	TextureShared64x1024,
	TextureShared64x512,
	TextureShared64x256,
	TextureShared64x256_3,
	TensorCores,
	TensorCoresShared64x512,
	TensorCoresShared32x128,
	TensorCoresShared16x32,
	SharedMemSample1,
	SharedMemSample2_coop16,
	SharedMemSample2_coop32,
	StrassenShared64,
	StrassenShared32,
	StrassenSharedNoTemp64,
	StrassenSharedNoTemp32,
	StrassenSharedShuffle64,
	StrassenSharedShuffle32,
	StrassenShared64_2,
	StrassenShared32_2,
	StrassenSharedNoTemp64_2,
	StrassenSharedNoTemp32_2,
	StrassenSharedShuffle64_2,
	StrassenSharedShuffle32_2,

	// Banded matrix multiply
	BandedNaive,
	BandedShiftMultiply64x1024,
	BandedShiftMultiply32x1024,
	BandedShiftMultiply64x512,
	BandedShiftMultiply32x512,
	BandedShiftMultiply64x256,
	BandedShiftMultiply32x256,
	BandedShiftMultiply64x1024_2,
	BandedShiftMultiply32x1024_2,
	BandedShiftMultiply64x512_2,
	BandedShiftMultiply32x512_2,
	BandedShiftMultiply64x256_2,
	BandedShiftMultiply32x256_2,
	BandedTensorMultiply64x512,
	BandedTensorMultiply32x128,
	BandedTensorMultiply16x32
};

static constexpr std::array<std::string_view, 46> ModeName = {
	"Naive"sv,
	"SharedMem"sv,
	"GlobalShared64x1024"sv,
	"GlobalShared64x512"sv,
	"GlobalShared64x256"sv,
	"GlobalShared76x152"sv,
	"GlobalShared76x304"sv,
	"TextureShared64x1024"sv,
	"TextureShared64x512"sv,
	"TextureShared64x256"sv,
	"TextureShared64x256_3"sv,
	"TensorCores"sv,
	"TensorCoresShared64x512"sv,
	"TensorCoresShared32x128"sv,
	"TensorCoresShared16x32"sv,
	"SharedMemSample1"sv,
	"SharedMemSample2_coop16"sv,
	"SharedMemSample2_coop32"sv,
	"StrassenShared64"sv,
	"StrassenShared32"sv,
	"StrassenSharedNoTemp64"sv,
	"StrassenSharedNoTemp32"sv,
	"StrassenSharedShuffle64"sv,
	"StrassenSharedShuffle32"sv,
	"StrassenShared64_2"sv,
	"StrassenShared32_2"sv,
	"StrassenSharedNoTemp64_2"sv,
	"StrassenSharedNoTemp32_2"sv,
	"StrassenSharedShuffle64_2"sv,
	"StrassenSharedShuffle32_2"sv,

	"BandedNaive"sv,
	"BandedShiftMultiply64x1024"sv,
	"BandedShiftMultiply32x1024"sv,
	"BandedShiftMultiply64x512"sv,
	"BandedShiftMultiply32x512"sv,
	"BandedShiftMultiply64x256"sv,
	"BandedShiftMultiply32x256"sv,
	"BandedShiftMultiply64x1024_2"sv,
	"BandedShiftMultiply32x1024_2"sv,
	"BandedShiftMultiply64x512_2"sv,
	"BandedShiftMultiply32x512_2"sv,
	"BandedShiftMultiply64x256_2"sv,
	"BandedShiftMultiply32x256_2"sv,
	"BandedTensorMultiply64x512"sv,
	"BandedTensorMultiply32x128"sv,
	"BandedTensorMultiply16x32"sv
};

enum Timer {
	data_copy = 0,
	compute,
	result_copy,
	count
};

static const char* TimerName[] = {
	"data_copy",
	"compute",
	"result_copy",
};

#ifdef USETIMERS
__device__ unsigned long long int cuda_timers[Timer::count];
#define TIMER_TIC(timer) unsigned long long int tic_##timer; if (threadIdx.x == 0) tic_##timer = clock64();
#define TIMER_TOC(timer) if (threadIdx.x == 0) atomicAdd(&cuda_timers[timer], clock64() - tic_##timer);
#else
#define TIMER_TIC(timer)
#define TIMER_TOC(timer)
#endif

struct Result
{
	Mode mode;
	long long testRunTime = 0;
	int grid_size = -1;
	std::array<unsigned long long int, Timer::count> cuda_timers__H;
};

class AutoTimer
{
public:
	AutoTimer(const Mode mode, Result& result)
		: start{ std::chrono::high_resolution_clock::now() }
		, res{ result }
	{
		res.mode = mode;
	}

	~AutoTimer()
	{
		auto end = std::chrono::high_resolution_clock::now();
		res.testRunTime = (end - start).count() / 1000;

#ifdef USETIMERS
		cux(cudaMemcpyFromSymbol(res.cuda_timers__H.data(), cuda_timers, sizeof(cuda_timers)));
		unsigned long long int resetTimers[Timer::count] = {};
		cux(cudaMemcpyToSymbol(cuda_timers, resetTimers, sizeof(cuda_timers)));
#endif
	}

	void reset()
	{
		start = std::chrono::high_resolution_clock::now();
	}

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
	Result& res;
};

struct BMResult
{
	std::unordered_map<Mode, std::vector<Result>> timings;
	std::mutex mutex;
};

class TestBase
{
public:
	virtual void RunTests(const int deviceIndex, const int iterations, const Mode test, const bool benchmark, const cudaStream_t stream, BMResult& results) = 0;
	virtual size_t GetFloatingOperations() const = 0;

protected:
	bool warmupDone = false;
};
