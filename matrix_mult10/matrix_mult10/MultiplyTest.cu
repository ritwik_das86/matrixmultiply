#include "MultiplyTest.h"
#include "Matrix.h"
#include <cassert>
#include <chrono>
#include <device_launch_parameters.h>
#include <cooperative_groups.h>

template<typename _Ty> texture<_Ty, cudaTextureType2D> MultiplyTest<_Ty>::texA2;
template<typename _Ty> texture<_Ty, cudaTextureType2D> MultiplyTest<_Ty>::texB2;

template<typename _Ty>
__global__ void
__launch_bounds__(1024, 2)
multiplyNaive(
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	const int total,
	_Ty* __restrict__ c)
{
	const int id = blockDim.x * blockIdx.x + threadIdx.x;
	if (id >= total)
		return;

	c[id] = calculateCellValue<0, 0>(wa, a, wa, b, wb, id / wb, id % wb);
}

template<typename _Ty, int TILE_DIM>
__global__ void
__launch_bounds__(1024, 2)
multiplySharedMem(
	const _Ty* __restrict__ a, const int wa,
	const _Ty* __restrict__ b, const int wb,
	_Ty* __restrict__ c)
{
	const int tid = (threadIdx.y * TILE_DIM) + threadIdx.x;
	const int rc = (blockIdx.y * TILE_DIM) + threadIdx.y;
	const int cc = (blockIdx.x * TILE_DIM) + threadIdx.x;

	__shared__ _Ty rolcolBuf[2 * TILE_DIM * TILE_DIM]; // 1k for each a, b
	_Ty* rowsA = rolcolBuf;
	_Ty* colsB = rolcolBuf + TILE_DIM * TILE_DIM;

	_Ty s{};

	const int rowOffset = threadIdx.y * TILE_DIM;
	const int colOffset = threadIdx.x;
	const int nTiles = ceilDiv(wa, TILE_DIM);
	for (int tile = 0; tile < nTiles; ++tile)
	{
		TIMER_TIC(data_copy);

		// 1. copy the data required for this tile from global --> shared memory
		const int p = tile * TILE_DIM;
		const int ca = p + threadIdx.x;
		rowsA[tid] = getVal(a, rc, ca, wa);

		const int rb = p + threadIdx.y;
		colsB[tid] = getVal(b, rb, cc, wb);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// 2. Calculate the values for this tile
		for (int k = 0; k < TILE_DIM; ++k)
		{
			s += rowsA[rowOffset + k] * colsB[colOffset + (k * TILE_DIM)];
		}

		TIMER_TOC(compute);

		if (tile < nTiles - 1)
			__syncthreads();
	}

	TIMER_TIC(result_copy);

	// 3. copy result in shared mem to global memory
	c[rc * wb + cc] = s;

	TIMER_TOC(result_copy);
}

template<typename _Ty, int BLOCK_SIZE>
__device__ void matrixMulCUDA_coop(_Ty* __restrict__ C, const _Ty* __restrict__ A, const _Ty* __restrict__ B, const int wA, const int wB)
{
	// Handle to thread block group
	cooperative_groups::thread_block cta = cooperative_groups::this_thread_block();
	// Block index
	int bx = blockIdx.x;
	int by = blockIdx.y;

	// Thread index
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	// Index of the first sub-matrix of A processed by the block
	int aBegin = wA * BLOCK_SIZE * by;

	// Index of the last sub-matrix of A processed by the block
	int aEnd = aBegin + wA - 1;

	// Step size used to iterate through the sub-matrices of A
	int aStep = BLOCK_SIZE;

	// Index of the first sub-matrix of B processed by the block
	int bBegin = BLOCK_SIZE * bx;

	// Step size used to iterate through the sub-matrices of B
	int bStep = BLOCK_SIZE * wB;

	// Csub is used to store the element of the block sub-matrix
	// that is computed by the thread
	_Ty Csub{};

	// Loop over all the sub-matrices of A and B
	// required to compute the block sub-matrix
	for (int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep)
	{
		// Declaration of the shared memory array As used to
		// store the sub-matrix of A
		__shared__ _Ty As[BLOCK_SIZE][BLOCK_SIZE];

		// Declaration of the shared memory array Bs used to
		// store the sub-matrix of B
		__shared__ _Ty Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load the matrices from device memory
		// to shared memory; each thread loads
		// one element of each matrix
		As[ty][tx] = A[a + wA * ty + tx];
		Bs[ty][tx] = B[b + wB * ty + tx];

		// Synchronize to make sure the matrices are loaded
		cooperative_groups::sync(cta);


		// Multiply the two matrices together;
		// each thread computes one element
		// of the block sub-matrix
#pragma unroll
		for (int k = 0; k < BLOCK_SIZE; ++k)
		{
			Csub += As[ty][k] * Bs[k][tx];
		}

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		cooperative_groups::sync(cta);

	}

	// Write the block sub-matrix to device memory;
	// each thread writes one element
	int c = wB * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + wB * ty + tx] = Csub;
}

template<typename _Ty>
__global__ void  matrixMulCUDA_block16_coop(_Ty* __restrict__ C, const _Ty* __restrict__ A, const _Ty* __restrict__ B, const int wA, const int wB)
{
	matrixMulCUDA_coop<_Ty, 16>(C, A, B, wA, wB);
}

template<typename _Ty>
__global__ void  matrixMulCUDA_block32_coop(_Ty* __restrict__ C, const _Ty* __restrict__ A, const _Ty* __restrict__ B, const int wA, const int wB)
{
	matrixMulCUDA_coop<_Ty, 32>(C, A, B, wA, wB);
}

template<typename _Ty, int BLOCK_SIZE>
__global__ void MatrixMulCUDA(_Ty *C, const _Ty *A, const _Ty *B, const int wA, const int wB)
{
	// Block index
	int bx = blockIdx.x;
	int by = blockIdx.y;

	// Thread index
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	// Index of the first sub-matrix of A processed by the block
	int aBegin = wA * BLOCK_SIZE * by;

	// Index of the last sub-matrix of A processed by the block
	int aEnd = aBegin + wA - 1;

	// Step size used to iterate through the sub-matrices of A
	int aStep = BLOCK_SIZE;

	// Index of the first sub-matrix of B processed by the block
	int bBegin = BLOCK_SIZE * bx;

	// Step size used to iterate through the sub-matrices of B
	int bStep = BLOCK_SIZE * wB;

	// Csub is used to store the element of the block sub-matrix
	// that is computed by the thread
	_Ty Csub{};

	// Loop over all the sub-matrices of A and B
	// required to compute the block sub-matrix
	for (int a = aBegin, b = bBegin;
		a <= aEnd;
		a += aStep, b += bStep) {
		// Declaration of the shared memory array As used to
		// store the sub-matrix of A
		__shared__ _Ty As[BLOCK_SIZE][BLOCK_SIZE];

		// Declaration of the shared memory array Bs used to
		// store the sub-matrix of B
		__shared__ _Ty Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load the matrices from device memory
		// to shared memory; each thread loads
		// one element of each matrix
		As[ty][tx] = A[a + wA * ty + tx];
		Bs[ty][tx] = B[b + wB * ty + tx];

		// Synchronize to make sure the matrices are loaded
		__syncthreads();

		// Multiply the two matrices together;
		// each thread computes one element
		// of the block sub-matrix
#pragma unroll

		for (int k = 0; k < BLOCK_SIZE; ++k) {
			Csub += As[ty][k] * Bs[k][tx];
		}

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}

	// Write the block sub-matrix to device memory;
	// each thread writes one element
	int c = wB * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + wB * ty + tx] = Csub;
}

// Helper function for using CUDA to add vectors in parallel.
template<typename _Ty>
Result MultiplyTest<_Ty>::selector(const Mode mode, const int deviceIndex, const bool benchmark)
{
	assert(a.width() == b.height());
	assert(a.height() == c.height());
	assert(c.width() == b.width());

	// Launch a kernel on the GPU with one thread for each element.
	const int threadsPerBlock = 1024;
	const int totalItems = c.height() * c.width();

	Result res;
	{
		AutoTimer timer(mode, res);

		switch (mode)
		{
		case Mode::Naive:
		{
			const int threads = totalItems;
			res.grid_size = ceilDiv(threads, threadsPerBlock);
			// Save result to baseline for comparison with other kernels
			multiplyNaive<<<res.grid_size, threadsPerBlock>>>(a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), totalItems, c.getDevicePtr());
		}
		break;
		case Mode::SharedMem:
		{
			const int threads = 32;
			const dim3 threadDim(threads, threads);
			const dim3 gridDim(ceilDiv(c.width(), threads), ceilDiv(c.height(), threads));
			res.grid_size = gridDim.x * gridDim.y;
			//std::cout << "Mode::SharedMem: " << gridDim.x << "x" << gridDim.y << " blocks, " << threadsPerBlock << " threads per block. ";
			multiplySharedMem<_Ty, 32><<<gridDim, threadDim>>>(a.getDevicePtr(), a.width(), b.getDevicePtr(), b.width(), c.getDevicePtr());
		}
		break;
		case Mode::GlobalShared64x1024:
			res.grid_size = GlobalSharedKernelHelper<64, 1024>(timer, deviceIndex);
			break;
		case Mode::GlobalShared64x512:
			res.grid_size = GlobalSharedKernelHelper<64, 512>(timer, deviceIndex);
			break;
		case Mode::GlobalShared64x256:
			res.grid_size = GlobalSharedKernelHelper<64, 256>(timer, deviceIndex);
			break;
		case Mode::GlobalShared76x152:
			res.grid_size = GlobalSharedKernelHelper<76, 152>(timer, deviceIndex);
			break;
		case Mode::GlobalShared76x304:
			res.grid_size = GlobalSharedKernelHelper<76, 304>(timer, deviceIndex);
			break;
		case Mode::TextureShared64x1024:
			res.grid_size = TextureSharedKernelHelper<64, 1024, 2>(timer, deviceIndex);
			break;
		case Mode::TextureShared64x512:
			res.grid_size = TextureSharedKernelHelper<64, 512, 2>(timer, deviceIndex);
			break;
		case Mode::TextureShared64x256:
			res.grid_size = TextureSharedKernelHelper<64, 256, 2>(timer, deviceIndex);
			break;
		case Mode::TextureShared64x256_3:
			// This kernel is worse in performance than TextureShared64x256 because pushing 1 more block on the SM causes the following problems:
			// 1. The texture load performance becomes ~19 transactions/req as compared to 16 for only 2 blocks (TextureShared64x256)
			// 2. L2 cache hit rate also goes down to ~26% (loads) from 51% (TextureShared64x256) and 35% (GlobalShared64x256)
			res.grid_size = TextureSharedKernelHelper<64, 256, 3>(timer, deviceIndex);
			break;
		case Mode::TensorCores:
			res.grid_size = TensorCoresBasic();
			break;
		case Mode::TensorCoresShared64x512:
			res.grid_size = TensorMultiplyHelper<64, half>(timer, deviceIndex);
			break;
		case Mode::TensorCoresShared32x128:
			res.grid_size = TensorMultiplyHelper<32, half>(timer, deviceIndex);
			break;
		case Mode::TensorCoresShared16x32:
			res.grid_size = TensorMultiplyHelper<16, half>(timer, deviceIndex);
			break;
		case Mode::SharedMemSample1:
			{
				dim3 threads(32, 32);
				dim3 grid(b.width() / threads.x, a.height() / threads.y);
				res.grid_size = grid.x * grid.y;
				MatrixMulCUDA<_Ty, 32><<<grid, threads>>>(c.getDevicePtr(), a.getDevicePtr(), b.getDevicePtr(), a.width(), b.width());
			}
			break;
		case Mode::SharedMemSample2_coop16:
			{
				dim3 threads(16, 16);
				dim3 grid(b.width() / threads.x, a.height() / threads.y);
				res.grid_size = grid.x * grid.y;
				matrixMulCUDA_block16_coop<_Ty><<<grid, threads>>>(c.getDevicePtr(), a.getDevicePtr(), b.getDevicePtr(), a.width(), b.width());
			}
			break;
		case Mode::SharedMemSample2_coop32:
			{
				dim3 threads(32, 32);
				dim3 grid(b.width() / threads.x, a.height() / threads.y);
				res.grid_size = grid.x * grid.y;
				matrixMulCUDA_block32_coop<_Ty><<<grid, threads>>>(c.getDevicePtr(), a.getDevicePtr(), b.getDevicePtr(), a.width(), b.width());
			}
			break;
		case Mode::StrassenShared64:
			res.grid_size = StrassenMultiplyHelper<_Ty, 0, 64>(timer, deviceIndex);
			break;
		case Mode::StrassenShared32:
			res.grid_size = StrassenMultiplyHelper<_Ty, 0, 32>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedNoTemp64:
			res.grid_size = StrassenMultiplyHelper<_Ty, 1, 64>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedNoTemp32:
			res.grid_size = StrassenMultiplyHelper<_Ty, 1, 32>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedShuffle64:
			res.grid_size = StrassenMultiplyHelper<_Ty, 2, 64>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedShuffle32:
			res.grid_size = StrassenMultiplyHelper<_Ty, 2, 32>(timer, deviceIndex);
			break;
		case Mode::StrassenShared64_2:
			res.grid_size = StrassenMultiplyHelper<half, 0, 64>(timer, deviceIndex);
			break;
		case Mode::StrassenShared32_2:
			res.grid_size = StrassenMultiplyHelper<half, 0, 32>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedNoTemp64_2:
			res.grid_size = StrassenMultiplyHelper<half, 1, 64>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedNoTemp32_2:
			res.grid_size = StrassenMultiplyHelper<half, 1, 32>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedShuffle64_2:
			res.grid_size = StrassenMultiplyHelper<half, 2, 64>(timer, deviceIndex);
			break;
		case Mode::StrassenSharedShuffle32_2:
			res.grid_size = StrassenMultiplyHelper<half, 2, 32>(timer, deviceIndex);
			break;
		default:
			break;
		}

		// Check for any errors launching the kernel
		// cux(cudaGetLastError());

		// cudaDeviceSynchronize waits for the kernel to finish, and returns
		// any errors encountered during the launch.
		if (!benchmark)
			cux(cudaDeviceSynchronize());
	}

	return res;
}

template<typename _Ty>
void MultiplyTest<_Ty>::RunTests(const int deviceIndex, const int iterations, const Mode test, const bool benchmark, const cudaStream_t stream, BMResult& results)
{
	/*std::cout << "Left:\n";
	a.print();
	std::cout << "Right:\n";
	b.print();*/

	// Run warmup kernel
	if (!benchmark && !warmupDone)
	{
		Result warmup = selector(Mode::Naive, deviceIndex, benchmark);
		std::cout << "Warmup time: " << warmup.testRunTime << "us.\n";
		warmupDone = true;
		baseline = c;
	}

	// Run multiple iterations on each device
	for (int it = 0; it < iterations; ++it)
	{
		Result res = selector(test, deviceIndex, benchmark);
		if (!benchmark)
		{
			if (res.grid_size != -1)
			{
				std::cout.precision(7);
				std::cout << "Kernel time (" << ModeName[(int)test] << ", " << deviceIndex << "): " << res.testRunTime << "us. Error percent: " << std::fixed << (baseline == c) << "%" << std::endl;

				for (auto& timer : res.cuda_timers__H)
				{
					timer /= res.grid_size;
				}

				std::lock_guard<std::mutex> guard(results.mutex);
				results.timings[test].push_back(res);
			}

			// No need to clear when running benchmark
			c.reset();
		}
	}
}

//template class MultiplyTest<int>;
//template class MultiplyTest<half>;
template class MultiplyTest<float>;