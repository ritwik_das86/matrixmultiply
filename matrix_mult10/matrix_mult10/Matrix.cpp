#include "Matrix.h"
#include "TestBase.h"

template<typename _Ty>
Matrix<_Ty>::Matrix(const int height, const int width, bool clear)
	: W{ width }
	, H{ height }
{
	//std::cout << "Create matrix (" << width << "x" << height << ")\n";

	srand(static_cast<unsigned int>(time(NULL)));
	std::mt19937 gen;
	gen.seed(rand());

	const int items = width * height;
	const size_t size = items * sizeof(_Ty);
	cux(cudaMalloc(&mat__D, size));
	if (clear)
	{
		cux(cudaMemset(mat__D, 0, size));
	}
	else
	{
		mat = new _Ty[items];
		for (int i = 0; i < items; ++i)
		{
			if constexpr (!std::is_integral_v<_Ty>)
			{
				const float val = static_cast<float>(gen()) / std::mt19937::max() * 2;
				if (std::is_same_v<_Ty, half>)
					mat[i] = __float2half_rn(val);
				else
					mat[i] = val;
			}
			else
			{
				mat[i] = static_cast<_Ty>(gen());
			}
		}

		cux(cudaMemcpy(mat__D, mat, size, cudaMemcpyHostToDevice));
	}
}

template<typename _Ty>
Matrix<_Ty>::~Matrix()
{
	if (mat)
		delete[] mat;
	cux(cudaFree(mat__D));
}

template<typename _Ty>
Matrix<_Ty>& Matrix<_Ty>::operator=(const Matrix<_Ty>& other) noexcept
{
	assert(W == other.W);
	assert(H == other.H);

	if (mat != nullptr && other.mat != nullptr)
		memcpy(mat, other.mat, W * H * sizeof(_Ty));

	if (mat__D != nullptr && other.mat__D != nullptr)
		cux(cudaMemcpy(mat__D, other.mat__D, W * H * sizeof(_Ty), cudaMemcpyDeviceToDevice));

	return *this;
}

template<typename _Ty>
double Matrix<_Ty>::operator==(Matrix& other)
{
	HydrateFromDevice();
	other.HydrateFromDevice();

	const double res = compare(other);

	if (res > 0.1)
	{
		/*std::cout << "Baseline:\n";
		print();
		std::cout << "Result:\n";
		other.print();*/

		/*for (int i = 0; i < W * H; ++i)
		{
			if (mat[i] != other.mat[i])
				std::cout << i / W << ", "<< i % W << ": " << mat[i] << " " << other.mat[i] << std::endl;
		}*/
	}
	return res;
}

//template<typename _Ty>
//void Matrix<_Ty>::multiply(const Matrix& other, Matrix& result) const
//{
//	result.mat = new _Ty[result.height() * result.width()];
//	memset(result.mat, 0, result.height() * result.width() * sizeof(_Ty));
//	for (int i = 0; i < result.height(); ++i)
//	{
//		for (int j = 0; j < result.width(); ++j)
//		{
//			for (int k = 0; k < W; ++k)
//			{
//				result.mat[i * result.W + j] += getVal(mat, i, k, W) * getVal(other.mat, k, j, other.W);
//			}
//		}
//	}
//}

template<typename _Ty>
void Matrix<_Ty>::print()
{
	if (mat == nullptr)
		HydrateFromDevice();

	for (int i = 0; i < H; ++i)
	{
		std::cout << "[ ";
		for (int j = 0; j < W; ++j)
		{
			const _Ty val = getVal(mat, i, j, W);
			if constexpr (std::is_same_v<_Ty, half>)
			{
				std::cout << __half2float(val) << " ";
			}
			else
			{
				std::cout << val << " ";
			}
		}
		std::cout << "]\n";
	}
}

template<typename _Ty>
void Matrix<_Ty>::HydrateFromDevice()
{
	if (!mat)
	{
		mat = new _Ty[W * H];
	}

	cux(cudaMemcpy(mat, mat__D, W * H * sizeof(_Ty), cudaMemcpyDeviceToHost));
}

template<typename _Ty>
void Matrix<_Ty>::reset()
{
	cux(cudaMemset(mat__D, 0, W * H * sizeof(_Ty)));
	if (mat != nullptr)
		memset(mat, 0, W * H * sizeof(_Ty));
}

template<typename _Ty>
double Matrix<_Ty>::compare(Matrix& other) const
{
	double total_diff = 0.0;
	for (int i = 0; i < W * H; ++i)
	{
		const _Ty d = mat[i] - other.mat[i];
		double diff = d;
		double base = mat[i];
		double cmp = other.mat[i];
		if constexpr (std::is_same_v<_Ty, half>)
		{
			diff = __half2float(d);
			base = __half2float(mat[i]);
			cmp = __half2float(other.mat[i]);
		}

		/*if (diff != 0)
			std::cout << "diff: " << diff << ", base: " << base << ", other: " << cmp << std::endl;*/

		total_diff += abs(diff) / base;
	}

	return total_diff / (W * H) * 100;
}

template class Matrix<int>;
template class Matrix<float>;
template class Matrix<half>;