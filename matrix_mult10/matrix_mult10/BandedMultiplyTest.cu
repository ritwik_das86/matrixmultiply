#include "BandedMultiplyTest.h"
#include "Matrix.h"
#include <mma.h>

template<typename _Ty>
__global__ void BandedMultiplyNaive(
	_Ty* __restrict__ t0,
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2)
{
	const int id = blockDim.x * blockIdx.x + threadIdx.x;
	const int rc = id / N1;
	const int cc = id % N1;

	_Ty s{};

	TIMER_TIC(compute);

	for (int b = 0; b < N2; ++b)
	{
		s += getVal<N2>(t1, rc, b) * getVal<N1>(t2, rc + b, cc);
		/*if (id == 0)
		{
			printf("%f x %f, s: %d\n", getVal(t1, rc, b, N2), getVal(t2, rc + b, cc, N1), s);
		}*/
	}

	TIMER_TOC(compute);

	TIMER_TIC(result_copy);

	t0[id] += s;

	TIMER_TOC(result_copy);

	/*if (id == 0)
	{
		printf("t0[0]: %f", t0[0]);
	}*/
}

template<int TILE, int BLOCK_SIZE, int TILE_DIM, int STRIDE, int NTILES, typename _Ty, typename _Uy>
__device__ void ComputeTile(
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	_Uy* __restrict__ rowsT1,
	_Uy* __restrict__ colsT2,
	_Ty* __restrict__ sum,
	const int tOffset,
	const int rc,
	const int x,
	pipeline& pipe)
{
	TIMER_TIC(data_copy);

	// Load the matrix data into shared memory
	CopyMatrixDataToSharedMem<BLOCK_SIZE, TILE_DIM, STRIDE, TILE, NTILES>(t1, t2, rowsT1, colsT2, pipe);

	TIMER_TOC(data_copy);

	__syncthreads();

	TIMER_TIC(compute);

	// Multiply the 2 matrices
	MultiplyTile<TILE_DIM, BLOCK_SIZE>(threadIdx.x, rowsT1, colsT2, sum);

	TIMER_TOC(compute);

	if (TILE < NTILES - 1) // No need to sync after the last tile
		__syncthreads();
}

template<int BLOCK_SIZE, int TILE_DIM, int STRIDE, int NTILES, typename _Ty, typename _Uy>
__device__ void ComputeTile(
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	_Uy* __restrict__ rowsT1,
	_Uy* __restrict__ colsT2,
	_Ty* __restrict__ sum,
	const int tOffset,
	const int rc,
	const int x,
	const int tile,
	pipeline& pipe)
{
	TIMER_TIC(data_copy);

	// Load the matrix data into shared memory
	CopyMatrixDataToSharedMem<BLOCK_SIZE, TILE_DIM, STRIDE>(t1, t2, rowsT1, colsT2, tile, pipe);

	TIMER_TOC(data_copy);

	__syncthreads();

	TIMER_TIC(compute);

	// Multiply the 2 matrices
	MultiplyTile<TILE_DIM, BLOCK_SIZE>(threadIdx.x, rowsT1, colsT2, sum);

	TIMER_TOC(compute);

	if (tile < NTILES - 1) // No need to sync after the last tile
		__syncthreads();
}

template<typename _Ty, int TILE_DIM, int BLOCK_SIZE, int MIN_BLOCKS, typename _Uy = _Ty, int TILE_SIZE = TILE_DIM * TILE_DIM, int STRIDE = TILE_SIZE / BLOCK_SIZE, int NTILES = N2 / TILE_DIM + 1>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
BandedShiftedMultiply(
	_Ty* __restrict__ t0,
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	const _Ty* __restrict__ baseline)
{
	__shared__ _Uy rowsT1[TILE_SIZE];
	__shared__ _Uy colsT2[TILE_SIZE];

	_Ty sum[STRIDE] = {};

	pipeline pipe;

	const int rc = blockIdx.y * TILE_DIM;
	const int cc = blockIdx.x * TILE_DIM;
	const int tOffset = threadIdx.x % TILE_DIM;
	const int x = cc + tOffset;

	// since the band width is N2, for every tile
	// of the output matrix we need to sweep (N2 / TILE_DIM) + 1 tiles
	if (NTILES == 17)
	{
		ComputeTile<0, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<1, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<2, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<3, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<4, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<5, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<6, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<7, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<8, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<9, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<10, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<11, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<12, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<13, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<14, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<15, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<16, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
	}
	else if (NTILES == 33)
	{
		ComputeTile<0, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<1, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<2, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<3, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<4, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<5, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<6, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<7, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<8, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<9, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<10, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<11, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<12, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<13, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<14, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<15, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<16, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<17, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<18, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<19, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<20, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<21, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<22, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<23, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<24, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<25, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<26, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<27, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<28, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<29, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<30, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<31, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
		ComputeTile<32, BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, pipe);
	}
	else
	{
		for (int tile = 0; tile < NTILES; ++tile)
		{
			ComputeTile<BLOCK_SIZE, TILE_DIM, STRIDE, NTILES>(t1, t2, rowsT1, colsT2, sum, tOffset, rc, x, tile, pipe);
		}
	}

	TIMER_TIC(result_copy);

	// Copy data to global memory
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int row = rc + (p / TILE_DIM);
		const _Ty val = sum[i] + getVal<N1>(t0, row, x);
#ifdef _DEBUG
		if (std::is_integral<_Ty>::value)
		{
			const _Ty cmp = getVal<N1>(baseline, row, x);
			assert(cmp == val);
		}
#endif
		setVal<N1>(t0, row, x, val);
	}

	TIMER_TOC(result_copy);
}

template<
	typename _Ty,
	int TILE_DIM,
	int BLOCK_SIZE,
	int MIN_BLOCKS,
	typename _Uy,
	int TILE_SIZE = TILE_DIM * TILE_DIM,
	int STRIDE = TILE_SIZE / BLOCK_SIZE,
	int NTILES = N2 / TILE_DIM + 1,
	int M = 16,
	int N = 16,
	int K = 16>
__global__ void
__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
BandedTensorMultiply(
	_Ty* __restrict__ t0,
	const _Ty* __restrict__ t1,
	const _Ty* __restrict__ t2,
	const _Ty* __restrict__ baseline)
{
#if __CUDA_ARCH__ >= 700
	using namespace nvcuda;
	using a_fragment = typename wmma::fragment<wmma::matrix_a, 16, 16, 16, _Uy, wmma::row_major>;
	using b_fragment = typename wmma::fragment<wmma::matrix_b, 16, 16, 16, _Uy, wmma::row_major>;
	using acc_fragment = typename wmma::fragment<wmma::accumulator, 16, 16, 16, _Ty>;

	__shared__ _Uy rowsT1[TILE_SIZE];
	__shared__ _Uy colsT2[TILE_SIZE];
	__shared__ _Ty result[TILE_SIZE];

	pipeline pipe;

	const int rc = blockIdx.y * TILE_DIM;
	const int cc = blockIdx.x * TILE_DIM;
	const int tOffset = threadIdx.x % TILE_DIM;
	const int x = cc + tOffset;

	TIMER_TIC(data_copy);
	// Copy from t0 to result
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int row = rc + (p / TILE_DIM);
		result[p] = getVal<N1>(t0, row, x);
	}
	TIMER_TOC(data_copy);

	a_fragment a_frag;
	b_fragment b_frag;
	acc_fragment ab_frag;
	acc_fragment c_frag;

	// since the band width is N2, for every tile
	// of the output matrix we need to sweep (N2 / TILE_DIM) + 1 tiles
	for (int tile = 0; tile < NTILES; ++tile)
	{
		TIMER_TIC(data_copy);

		// Load the matrix data into shared memory
		CopyMatrixDataToSharedMem<BLOCK_SIZE, TILE_DIM, STRIDE>(t1, t2, rowsT1, colsT2, tile, pipe);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// Multiply the 2 matrices
		const int tensorStride = TILE_DIM / 16 * warpSize;
		const int ix = (threadIdx.x % tensorStride) / warpSize;
		const int iy = threadIdx.x / tensorStride;

		wmma::fill_fragment(ab_frag, 0.0f);

		// AB = A*B
		const int a_row = iy * M;
		const int b_col = ix * N;
		//const int tileOffset = (blockIdx.y + tile) * TILE_DIM;
		for (int k = 0; k < TILE_DIM; k += K)
		{
			// Load the inputs
			wmma::load_matrix_sync(a_frag, rowsT1 + k + a_row * TILE_DIM, TILE_DIM);
			wmma::load_matrix_sync(b_frag, colsT2 + b_col + k * TILE_DIM, TILE_DIM);

			// Perform the matrix multiplication
			wmma::mma_sync(ab_frag, a_frag, b_frag, ab_frag);
		}

		// D = AB + C
		wmma::load_matrix_sync(c_frag, result + b_col + a_row * TILE_DIM, TILE_DIM, wmma::mem_row_major);
		for (int i = 0; i < c_frag.num_elements; i++)
		{
			c_frag.x[i] = ab_frag.x[i] + c_frag.x[i];
		}

		// Store the output
		wmma::store_matrix_sync(result + b_col + a_row * TILE_DIM, c_frag, TILE_DIM, wmma::mem_row_major);

		TIMER_TOC(compute);

		if (tile < NTILES - 1) // No need to sync after the last tile
			__syncthreads();
	}

	TIMER_TIC(result_copy);

	// Copy data to global memory
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int row = rc + (p / TILE_DIM);
		const _Ty val = result[p];
#ifdef _DEBUG
		if (std::is_integral<_Ty>::value)
		{
			const _Ty cmp = getVal<N1>(baseline, row, x);
			assert(cmp == val);
		}
#endif
		setVal<N1>(t0, row, x, val);
	}

	TIMER_TOC(result_copy);
#endif
}

template<
	typename _Ty,
	int TILE_DIM,
	int BLOCK_SIZE,
	typename _Uy = _Ty,
	int MIN_BLOCKS_OVERRIDE = 32,
	int SHAREDMEM_PER_BLOCK = 2 * TILE_DIM * TILE_DIM * sizeof(_Uy),
	int TOTAL_SHAREDMEM = 96 * 1024,
	int MAX_BLOCKS = (2048 / BLOCK_SIZE) < 32 ? (2048 / BLOCK_SIZE) : 32,
	int MAX_BLOCKS_BY_SHAREDMEM = TOTAL_SHAREDMEM / SHAREDMEM_PER_BLOCK,
	int MIN_BLOCKS_CALC = MAX_BLOCKS_BY_SHAREDMEM < MAX_BLOCKS ? MAX_BLOCKS_BY_SHAREDMEM : MAX_BLOCKS,
	int MIN_BLOCKS = MIN_BLOCKS_OVERRIDE < MIN_BLOCKS_CALC ? MIN_BLOCKS_OVERRIDE : MIN_BLOCKS_CALC,
	int REQUIRED_SHAREDMEM_PERCENT = MIN_BLOCKS * SHAREDMEM_PER_BLOCK * 100 / TOTAL_SHAREDMEM>
int BandedMultiplyHelper(Matrix<_Ty>& t0, const Matrix<_Ty>& t1, const Matrix<_Ty>& t2, const Matrix<_Ty>& baseline, const cudaStream_t stream)
{
	cux(cudaFuncSetAttribute(BandedShiftedMultiply<_Ty, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS, _Uy>, cudaFuncAttributePreferredSharedMemoryCarveout, REQUIRED_SHAREDMEM_PERCENT));
	//std::cout << "Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << MIN_BLOCKS << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
	const dim3 gridDim(t0.width() / TILE_DIM, t0.height() / TILE_DIM);
	BandedShiftedMultiply<_Ty, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS, _Uy><<<gridDim, BLOCK_SIZE, 0, stream>>>(t0.getDevicePtr(), t1.getDevicePtr(), t2.getDevicePtr(), baseline.getDevicePtr());

	return gridDim.x * gridDim.y;
}

template<
	typename _Ty,
	int TILE_DIM,
	typename _Uy,
	int MIN_BLOCKS_OVERRIDE = 32,
	int BLOCK_SIZE = TILE_DIM * TILE_DIM / (16 * 16) * 32, // a single warp computes a 16 x 16 tile
	int SHAREDMEM_PER_BLOCK = 2 * TILE_DIM * TILE_DIM * sizeof(_Uy) + TILE_DIM * TILE_DIM * sizeof(_Ty),
	int TOTAL_SHAREDMEM = 96 * 1024,
	int MAX_BLOCKS = (2048 / BLOCK_SIZE) < 32 ? (2048 / BLOCK_SIZE) : 32,
	int MAX_BLOCKS_BY_SHAREDMEM = TOTAL_SHAREDMEM / SHAREDMEM_PER_BLOCK,
	int MIN_BLOCKS_CALC = MAX_BLOCKS_BY_SHAREDMEM < MAX_BLOCKS ? MAX_BLOCKS_BY_SHAREDMEM : MAX_BLOCKS,
	int MIN_BLOCKS = MIN_BLOCKS_OVERRIDE < MIN_BLOCKS_CALC ? MIN_BLOCKS_OVERRIDE : MIN_BLOCKS_CALC,
	int REQUIRED_SHAREDMEM_PERCENT = MIN_BLOCKS * SHAREDMEM_PER_BLOCK * 100 / TOTAL_SHAREDMEM>
int BandedTensorMultiplyHelper(Matrix<_Ty>& t0, const Matrix<_Ty>& t1, const Matrix<_Ty>& t2, const Matrix<_Ty>& baseline, const cudaStream_t stream)
{
	if (std::is_integral<_Ty>::value)
		return -1;

	cux(cudaFuncSetAttribute(BandedShiftedMultiply<_Ty, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS, _Uy>, cudaFuncAttributePreferredSharedMemoryCarveout, REQUIRED_SHAREDMEM_PERCENT));
	//std::cout << "Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << MIN_BLOCKS << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
	const dim3 gridDim(t0.width() / TILE_DIM, t0.height() / TILE_DIM);
	BandedTensorMultiply<_Ty, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS, _Uy><<<gridDim, BLOCK_SIZE, 0, stream>>>(t0.getDevicePtr(), t1.getDevicePtr(), t2.getDevicePtr(), baseline.getDevicePtr());

	return gridDim.x * gridDim.y;
}

template<typename _Ty>
Result selector(Matrix<_Ty>& t0, const Matrix<_Ty>& t1, const Matrix<_Ty>& t2, const Matrix<_Ty>& baseline, const Mode mode, const int deviceIndex, const bool benchmark, const cudaStream_t stream)
{
	const int threadsPerBlock = 1024;
	const int totalItems = t0.height() * t0.width();

	Result res;
	{
		AutoTimer timer(mode, res);

		switch (mode)
		{
		case Mode::BandedNaive:
			res.grid_size = totalItems / threadsPerBlock;
			BandedMultiplyNaive<_Ty><<<res.grid_size, threadsPerBlock, 0, stream>>>(t0.getDevicePtr(), t1.getDevicePtr(), t2.getDevicePtr());
			break;

		case Mode::BandedShiftMultiply64x1024:
			res.grid_size = BandedMultiplyHelper<_Ty, 64, 1024>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply32x1024:
			res.grid_size = BandedMultiplyHelper<_Ty, 32, 1024>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply64x512:
			res.grid_size = BandedMultiplyHelper<_Ty, 64, 512>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply32x512:
			res.grid_size = BandedMultiplyHelper<_Ty, 32, 512>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply64x256:
			res.grid_size = BandedMultiplyHelper<_Ty, 64, 256>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply32x256:
			res.grid_size = BandedMultiplyHelper<_Ty, 32, 256, _Ty, 7>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply64x1024_2:
			res.grid_size = BandedMultiplyHelper<_Ty, 64, 1024, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply32x1024_2:
			res.grid_size = BandedMultiplyHelper<_Ty, 32, 1024, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply64x512_2:
			res.grid_size = BandedMultiplyHelper<_Ty, 64, 512, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply32x512_2:
			res.grid_size = BandedMultiplyHelper<_Ty, 32, 512, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply64x256_2:
			res.grid_size = BandedMultiplyHelper<_Ty, 64, 256, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedShiftMultiply32x256_2:
			res.grid_size = BandedMultiplyHelper<_Ty, 32, 256, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedTensorMultiply64x512:
			res.grid_size = BandedTensorMultiplyHelper<_Ty, 64, half>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedTensorMultiply32x128:
			res.grid_size = BandedTensorMultiplyHelper<_Ty, 32, half, 7>(t0, t1, t2, baseline, stream);
			break;

		case Mode::BandedTensorMultiply16x32:
			res.grid_size = BandedTensorMultiplyHelper<_Ty, 16, half, 26>(t0, t1, t2, baseline, stream);
			break;

		default:
			break;
		}

		if (!benchmark)
			cux(cudaDeviceSynchronize());
	}

	return res;
}

template<typename _Ty>
void BandedMultiplyTest<_Ty>::RunTests(const int deviceIndex, const int iterations, const Mode test, const bool benchmark, const cudaStream_t stream, BMResult& results)
{
	// Run warmup kernel
	if (!benchmark && !warmupDone)
	{
		Result warmup = selector(baseline, t1, t2, baseline, Mode::BandedNaive, deviceIndex, benchmark, stream);
		std::cout << "Warmup time: " << warmup.testRunTime << "us.\n";
		warmupDone = true;
	}

	// Run multiple iterations on each device
	for (int it = 0; it < iterations; ++it)
	{
		Result res = selector<_Ty>(t0, t1, t2, baseline, test, deviceIndex, benchmark, stream);
		if (!benchmark)
		{
			if (res.grid_size != -1)
			{
				std::cout.precision(7);
				std::cout << "Kernel time (" << ModeName[(int)test] << ", " << deviceIndex << "): " << res.testRunTime << "us. Error percent: " << std::fixed << (baseline == t0) << "%" << std::endl;
				//std::cout << "t0:\n"; t0.print();
				/*std::cout << "t1:\n"; t1.print();
				std::cout << "t2:\n"; t2.print();
				std::cout << "c:\n"; c1.print();*/

				for (auto& timer : res.cuda_timers__H)
				{
					timer /= res.grid_size;
				}

				std::lock_guard<std::mutex> guard(results.mutex);
				results.timings[test].push_back(res);
			}

			// No need to clear when running benchmark
			t0.reset();
		}
	}
}

template class BandedMultiplyTest<float>;
