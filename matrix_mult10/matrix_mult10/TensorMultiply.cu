﻿#include "MultiplyTest.h"
#include <mma.h>

// MMA matrix tile dimensions.
#define M 16
#define N 16
#define K 16
#define WMMA_M 16
#define WMMA_N 16
#define WMMA_K 16

template<
	typename _Ty,
	int TILE_DIM,
	int BLOCK_SIZE,
	int MIN_BLOCKS,
	typename _Uy,
	int TILE_SIZE = TILE_DIM * TILE_DIM,
	int STRIDE = TILE_SIZE / BLOCK_SIZE>
	__global__ void
	__launch_bounds__(BLOCK_SIZE, MIN_BLOCKS)
	TensorSharedMultiply(
		_Ty* __restrict__ c,
		const _Ty* __restrict__ a,
		const _Ty* __restrict__ b,
		const int wa,
		const int wb,
		const _Ty* __restrict__ baseline)
{
#if __CUDA_ARCH__ >= 700
	using namespace nvcuda;
	using a_fragment = typename wmma::fragment<wmma::matrix_a, WMMA_M, WMMA_N, WMMA_K, _Uy, wmma::row_major>;
	using b_fragment = typename wmma::fragment<wmma::matrix_b, WMMA_M, WMMA_N, WMMA_K, _Uy, wmma::row_major>;
	using acc_fragment = typename wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, _Ty>;

	__shared__ _Uy rowsA[TILE_SIZE];
	__shared__ _Uy colsB[TILE_SIZE];
	__shared__ _Ty result[TILE_SIZE];

	const int rc = blockIdx.y * TILE_DIM;
	const int cc = blockIdx.x * TILE_DIM;
	const int x = cc + (threadIdx.x % TILE_DIM);

	TIMER_TIC(data_copy);
	// Copy from c to result
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int row = rc + (p / TILE_DIM);
		result[p] = getVal(c, row, x, wb);
	}
	TIMER_TOC(data_copy);

	a_fragment a_frag;
	b_fragment b_frag;
	acc_fragment ab_frag;
	acc_fragment c_frag;

	// since the band width is N2, for every tile
	// of the output matrix we need to sweep (N2 / TILE_DIM) + 1 tiles
	const int nTiles = wa / TILE_DIM;
	for (int tile = 0; tile < nTiles; ++tile)
	{
		TIMER_TIC(data_copy);

		// Load the matrix data into shared memory
		MultiplyTest<_Ty>::CopyMatrixDataToSharedMem<false, BLOCK_SIZE, TILE_DIM, STRIDE>(a, b, rowsA, colsB, wa, wb, tile);

		TIMER_TOC(data_copy);

		__syncthreads();

		TIMER_TIC(compute);

		// Multiply the 2 matrices
		const int tensorStride = TILE_DIM / WMMA_K * warpSize;
		const int ix = (threadIdx.x % tensorStride) / warpSize;
		const int iy = threadIdx.x / tensorStride;

		wmma::fill_fragment(ab_frag, 0.0f);

		// AB = A*B
		const int a_row = iy * M;
		const int b_col = ix * N;
		for (int k = 0; k < TILE_DIM; k += K)
		{
			// Load the inputs
			wmma::load_matrix_sync(a_frag, rowsA + k + a_row * TILE_DIM, TILE_DIM);
			wmma::load_matrix_sync(b_frag, colsB + b_col + k * TILE_DIM, TILE_DIM);

			// Perform the matrix multiplication
			wmma::mma_sync(ab_frag, a_frag, b_frag, ab_frag);
		}

		// D = AB + C
		_Ty* result_frag = result + b_col + a_row * TILE_DIM;
		wmma::load_matrix_sync(c_frag, result_frag, TILE_DIM, wmma::mem_row_major);
		for (int i = 0; i < c_frag.num_elements; i++)
		{
			c_frag.x[i] = ab_frag.x[i] + c_frag.x[i];
		}

		// Store the output
		wmma::store_matrix_sync(result_frag, c_frag, TILE_DIM, wmma::mem_row_major);

		TIMER_TOC(compute);

		if (tile < nTiles - 1) // No need to sync after the last tile
			__syncthreads();
	}

	TIMER_TIC(result_copy);

	// Copy data to global memory
	for (int i = 0, p = threadIdx.x; i < STRIDE; ++i, p += BLOCK_SIZE)
	{
		const int row = rc + (p / TILE_DIM);
		const _Ty val = result[p];
#ifdef _DEBUG
		if (std::is_integral<_Ty>::value)
		{
			const _Ty cmp = getVal(baseline, row, x, wb);
			assert(cmp == val);
		}
#endif
		setVal(c, row, x, wb, val);
	}

	TIMER_TOC(result_copy);
#endif
}

template<
	typename _Ty,
	typename _Uy,
	int TILE_DIM,
	int BLOCK_SIZE,
	int SHAREDMEM_PER_BLOCK,
	int MIN_BLOCKS>
	int TensorMultiplyLauncher(AutoTimer& timer, Matrix<_Ty>& c, const Matrix<_Ty>& a, const Matrix<_Ty>& b, const Matrix<_Ty>& baseline, const int reqSharedMemPercent)
{
	cux(cudaFuncSetAttribute(TensorSharedMultiply<_Ty, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS, _Uy>, cudaFuncAttributePreferredSharedMemoryCarveout, reqSharedMemPercent));
	//std::cout << "Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << MIN_BLOCKS << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
	const dim3 gridDim(c.width() / TILE_DIM, c.height() / TILE_DIM);
	timer.reset();
	TensorSharedMultiply<_Ty, TILE_DIM, BLOCK_SIZE, MIN_BLOCKS, _Uy><<<gridDim, BLOCK_SIZE>>>(c.getDevicePtr(), a.getDevicePtr(), b.getDevicePtr(), a.width(), b.width(), baseline.getDevicePtr());
	return gridDim.x * gridDim.y;
}

template<typename _Ty>
template<
	int TILE_DIM,
	typename _Uy,
	int MIN_BLOCKS_OVERRIDE,
	int BLOCK_SIZE,
	int SHAREDMEM_PER_BLOCK>
int MultiplyTest<_Ty>::TensorMultiplyHelper(AutoTimer& timer, const int deviceIndex)
{
	if (std::is_integral<_Ty>::value)
		return -1;

	const auto& [minBlocks, reqSharedMemPercent] = CalcMinBlocks<MIN_BLOCKS_OVERRIDE, BLOCK_SIZE, SHAREDMEM_PER_BLOCK>(deviceIndex);

	switch (minBlocks)
	{
	case 2:
		return TensorMultiplyLauncher<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, SHAREDMEM_PER_BLOCK, 2>(timer, c, a, b, baseline, reqSharedMemPercent);
	case 8:
		return TensorMultiplyLauncher<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, SHAREDMEM_PER_BLOCK, 8>(timer, c, a, b, baseline, reqSharedMemPercent);
	case 16:
		return TensorMultiplyLauncher<_Ty, _Uy, TILE_DIM, BLOCK_SIZE, SHAREDMEM_PER_BLOCK, 16>(timer, c, a, b, baseline, reqSharedMemPercent);
	default:
		std::cout << "No appropriate kernel found (please add one). Tile dim: " << TILE_DIM << ", blocks size: " << BLOCK_SIZE << ", min blocks per SM: " << minBlocks << ", sharedmem per block: " << SHAREDMEM_PER_BLOCK << std::endl;
		return -1;
	}
}

template int MultiplyTest<float>::TensorMultiplyHelper<64, half>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::TensorMultiplyHelper<32, half>(AutoTimer& timer, const int deviceIndex);
template int MultiplyTest<float>::TensorMultiplyHelper<16, half>(AutoTimer& timer, const int deviceIndex);


// Performs an MxNxK GEMM (C=alpha*A*B + beta*C) assuming:
//  1) Matrices are packed in memory.
//  2) M, N and K are multiples of 16. 
//  3) Neither A nor B are transposed.
// Note: This is a less performant version of the compute_gemm kernel. It is designed for
//       demonstration purposes only to show the CUDA WMMA API use without relying on
//       availability of the shared memory.
__global__ void simple_wmma_gemm(const half* a, const half* b, float* c, int m_ld, int n_ld, int k_ld)
{
#if __CUDA_ARCH__ >= 700
	using namespace nvcuda;

	// Leading dimensions. Packed with no transpositions.
	int lda = m_ld;
	int ldb = k_ld;
	int ldc = n_ld;

	// Tile using a 2D grid
	int warpM = (blockIdx.x * blockDim.x + threadIdx.x) / warpSize;
	int warpN = (blockIdx.y * blockDim.y + threadIdx.y);

	// Declare the fragments
	wmma::fragment<wmma::matrix_a, WMMA_M, WMMA_N, WMMA_K, half, wmma::row_major> a_frag;
	wmma::fragment<wmma::matrix_b, WMMA_M, WMMA_N, WMMA_K, half, wmma::row_major> b_frag;
	wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, float> acc_frag;

	wmma::fill_fragment(acc_frag, 0.0f);

	// Loop over k
	for (int i = 0; i < k_ld; i += WMMA_K)
	{
		int aRow = warpM * WMMA_M;
		int bCol = warpN * WMMA_N;

		// Load the inputs
		wmma::load_matrix_sync(a_frag, a + i + aRow * lda, lda);
		wmma::load_matrix_sync(b_frag, b + bCol + i * ldb, ldb);

		// Perform the matrix multiplication
		wmma::mma_sync(acc_frag, a_frag, b_frag, acc_frag);
	}

	// Load in the current value of c
	int cCol = warpN * WMMA_N;
	int cRow = warpM * WMMA_M;

	if (cRow < m_ld && cCol < n_ld) {
		// Store the output
		wmma::store_matrix_sync(c + cCol + cRow * ldc, acc_frag, ldc, wmma::mem_row_major);
	}
#endif
}

__global__ void CopyAsHalf(const float* __restrict__ src1, half* __restrict__ dst1)
{
	const int tid = blockIdx.x * blockDim.x + threadIdx.x;
	dst1[tid] = src1[tid];
}

template<typename _Ty>
int MultiplyTest<_Ty>::TensorCoresBasic()
{
	if (std::is_integral<_Ty>::value)
		return -1;

	dim3 gridDim;
	dim3 blockDim;

	// blockDim.x must be a multple of warpSize
	// 128x4 means we have 16 warps and a block computes a 64x64 output tile
	blockDim.x = 128;
	blockDim.y = 4;

	gridDim.x = (a.height() + (WMMA_M * blockDim.x / 32 - 1)) / (WMMA_M * blockDim.x / 32);
	gridDim.y = (b.width() + WMMA_N * blockDim.y - 1) / (WMMA_N * blockDim.y);

	half* ahalf;
	half* bhalf;
	cux(cudaMalloc(&ahalf, a.width() * a.height() * sizeof(half)));
	cux(cudaMalloc(&bhalf, b.width() * b.height() * sizeof(half)));
	CopyAsHalf<<<a.width() * a.height() / 1024, 1024>>>(a.getDevicePtr(), ahalf);
	CopyAsHalf<<<b.width() * b.height() / 1024, 1024>>>(b.getDevicePtr(), bhalf);
	simple_wmma_gemm<<<gridDim, blockDim>>>(ahalf, bhalf, (float*)c.getDevicePtr(), a.height(), b.width(), a.width());

	return gridDim.x * gridDim.y;
}

template int MultiplyTest<float>::TensorCoresBasic();