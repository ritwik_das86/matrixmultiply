// Codegen.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>

void print_qoffset(std::ofstream& file, int block_size)
{
	int mask = ~(63);
	for (int i = 0; i < 4; ++i)
	{
		// Define the 2D array
		file << "\n__device__ __constant__ int b" << block_size << "_i" << i << "[4][" << block_size / 64 << "] = {\n";
		for (int q = 0; q < 4; ++q)
		{
			file << "\t{";
			for (int thread = 0; thread < block_size; thread += 64)
			{
				file << ((thread + q * 4 * block_size + i * block_size) & mask) << ", ";
			}
			file << "},\n";
		}

		file << "};\n";

		// define the helper function
		file << "\ntemplate<> __device__ inline int getQuadOffset<" << block_size << ", " << i << ">(const int tid, const int q) {\n";
		file << "\treturn b" << block_size << "_i" << i << "[q][tid >> 6];\n";
		file << "}\n";
	}
}

int main()
{
	std::ofstream file("../../matrix_mult10/Lookup.h");

	file << "#pragma once\n\n";

	file<<"#include \"cuda_runtime.h\"\n\n";

	file << "template<int BLOCK_SIZE, int QUAD> __device__ inline int getQuadOffset(const int tid, const int i);\n";
	print_qoffset(file, 1024);
	print_qoffset(file, 512);
	print_qoffset(file, 256);
}
